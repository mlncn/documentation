# Roadmap

There are four main components to the Drutopia project: the organization, the base distribution, the (first) platform cooperative, called Solidarity, and the Drutopia promotional website.

You can track our progress in more detail from our [Milestones page](https://gitlab.com/groups/drutopia/milestones).

## Drutopia Organization

### Incorporate as a Cooperative

**Completed**
* Adopt a Fiscal Sponsor
* Define membership

**In Progress**
* Decision making structure

**Planned**
* Cooperative Bylaws
* Incorporation location (ie: which country and state/province is best to incorporate into?)
* Cooperative application

## Drutopia Base Distribution

**Completed**
* User Research
* Information Architecture
* Actions
* Articles
* Blog Posts
* Campaigns
* Customizable homepage
* Events
* Groups
* Profiles
* Users
* Starter theme

**In Progress**
* Publish content to social media platforms

**Planned**

* On-site donations
* Resource Library

## Solidarity Platform Cooperative 1.0

**Completed**
* Research
* Information Architecture

**Planned**

* Articles
  * A contributor can relate an action to an article.
  * A contributor can relate a group to a an article.
  * A contributor can relate a campaign to an article.
* Blog Posts
  * A contributor can relate an action to a blog post.
  * A contributor can relate a group to a blog post.
  * A contributor can relate a campaign to a blog post.
* Campaigns
  * A contributor can relate an action to a campaign.
  * A contributor can relate a group to a campaign.

## drutopia.org upgrade

**Completed**
* Research
* Information Architecture
* Survey
* White Paper
* Contact

**In Progress**
* Copywriting
* Customized homepage
* Initial Articles
* Membership form
* Donation form
