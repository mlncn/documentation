Drutopia Points of Unity
========================

Drutopia is an initiative to revolutionize the way we build online tools. We're combining the principles of software freedom, community ownership, and intersectional politics to co-develop technologies that meet our needs and reflect our values.

As participants in the Drutopia initiative we agree to:

* Be inclusive regarding gender, gender identity, sexual orientation, ethnicity, ability, age, religion, geography, and class.
* Commit to protection of personal information and privacy and freedom from surveillance.
* Value collaboration and cooperation above competition.
* Place human needs over private profit.
* Foster non-hierarchical structures and collective decision-making.
