Creating a Distribution using Drutopia
======================================

The :doc:`Drutopia base distribution <drutopia>` is designed to make it easy to build your own distribution using as much or as little of Drutopia as you require.

See :doc:`about-drupal-distributions` for introductory material.

Proposing a New Distribution
----------------------------

A new Drutopia distribution should be proposed as an issue in the Drutopia Organization project. The proposal should include a suggested name, purpose and initial list of features.


Criteria
~~~~~~~~

A distribution qualifies for acceptance into the Drutopia ecosystem if it is:


* Appropriate : Meets the needs of a core Drutopia audience group (small budget-organization) and will build off of Drutopia Base

* Feasible : Can be implemented with tools presently available

* Resourced : Has a team capable and available to build and maintain the distribution


The final decision as to whether a distribution is added is made by the Leadership Team.

Design Features
---------------

Key design factors facilitate building distributions on Drutopia.

Standard components
^^^^^^^^^^^^^^^^^^^

The `Drutopia Core <https://github.com/drutopia/drutopia_core>`_ feature provides a standard set of site building components such as view modes, `paragraph <https://www.drupal.org/project/paragraphs>`_ types, fields, user roles, and more that are ready to use in custom features.

Separate repositories
^^^^^^^^^^^^^^^^^^^^^

Unlike in many distributions where all features are in a single repository, in Drutopia each feature has a distinct repository. This means it's easy to build a custom distribution using only the specific features you need. As your requirements evolve, you can return to add further Drutopia features.

Stand-alone base theme
^^^^^^^^^^^^^^^^^^^^^^

Built on the `Bulma CSS framework <https://bulma.io>`_, the `Octavia <https://github.com/drutopia/octavia>`_ theme provides integration for a standard set of Drutopia elements such as view modes that can be reused in custom distributions built on Drutopia.

However, Octavia and Bulma are in no way required. Distribution developers may select whatever theme approach they desire and refer to Octavia as a model for what needs theming.

Deciding Whether to Base your Distribution on Drutopia
------------------------------------------------------

Criteria
^^^^^^^^

Criteria to consider include:

* *How closely does your use case match the nonprofit/grassroots focus of Drutopia?* While Drutopia could be relevant to a broad range of use cases, several of the features are specifically aimed at the needs of nonprofit organizations and grassroots groups.
* *How prepared and/or well positioned are you to contribute back?* Drutopia is set up to encourage groups building off the platform to also help enhance the platform in ways that meet their needs.
* *How flexible are you to meeting your specific needs in ways that meet broadly shared requirements?* Drutopia features are built to meet the common requirements of many organizations.
* *Do you want ongoing updates?* There are two basic types of Drupal distributions: starter kits (designed to be installed only once and then the site is taken in its own direction) and full featured distributions (which will be maintained, as well as having new functionality added). Drutopia is a full featured distribution, so if you're building a starter kit it may not be the best choice.

Pros
^^^^

There are many potential advantages to building your distribution off of Drutopia, among them:

* *Out of the box functionality*. Some of your use cases may already be met by the growing list of Drutopia features.
* *Curated extensions*. The set of extensions included in Drutopia have already been evaluated and specifically selected.
* *Model features*. The existing Drutopia code base provides models of how to address many of the complexities of distribution development including default content, feature-specific user permissions, configuration overrides, configuration updates, and more.
* *Development documentation*. The Drutopia development documentation provides detailed guidelines for building features on the Drutopia model.
* *Updates*. Drutopia is actively maintained and updated.
* *Cooperative model*. Drutopia is structured on a cooperative model enabling members to both shape and benefit from development.

Cons
^^^^

At the same time, there are potential tradeoffs that should be carefully considered, including:

* *Alpha status*. Drutopia is in alpha and so may undergo changes as it stabilizes to reach a first full release.
* *Architectural decisions*. As is generally true of Drupal-based distributions, Drutopia brings with it a certain set of assumptions and structural decisions. While in many cases it's possible to override and customize, the features will be of most value when they can be used straight up.

Create an Install Profile
-------------------------

An install profile is a necessary piece of a custom distribution. See `How to Write a Drupal 8 Installation Profile <https://www.drupal.org/docs/8/creating-distributions/how-to-write-a-drupal-8-installation-profile>`_.

Sub-profile
^^^^^^^^^^^

Drupal core does not support inheritance in install profiles, meaning by default it's not possible to base one install profile on another.

That said, for the adventurous, there is a patch available to enable this functionality; see `Allow profiles to provide a base/parent profile and load them in the correct order <https://www.drupal.org/project/drupal/issues/1356276>`_.

Stand-alone profile
^^^^^^^^^^^^^^^^^^^

The default option is to build a stand-alone install profile. The easiest way to do so is to crib from the Drutopia install profile itself.

Create a Theme
--------------

Using Octavia
^^^^^^^^^^^^^

For the simplest sub-distributions, it might be sufficient to use the Octavia theme directly::

    composer require drupal/octavia

Subtheming Octavia
^^^^^^^^^^^^^^^^^^

In most cases, the most effective starting place will be to sub-theme the Octavia theme that ships with Drutopia.

Install Octavia::

    composer require drupal/octavia

Then follow the documentation for `Creating a Drupal 8 sub-theme <https://www.drupal.org/docs/8/theming-drupal-8/creating-a-drupal-8-sub-theme-or-sub-theme-of-sub-theme>`_.

Writing a custom theme
^^^^^^^^^^^^^^^^^^^^^^

You can also, of course, write a custom theme. In doing so, you'll want to design for the components used in Drutopia, such as view modes.

You'll also need to provide some or all of the block configuration that's provided in the Octavia theme; see that theme's ``config/optional`` directory.

You may also wish to look at the Twig templates included with Octavia, as some may be relevant to  your custom theme.

Select and Install Drutopia Features
------------------------------------

Use composer to require an individual Drutopia feature like any other Drupal plugin. For example, for ``drutopia_core``::

    composer require drupal/drutopia_core

As a minimum, include the following features:

* Drutopia Core
* Drutopia Site

Most sites will also want some or all of:

* Drutopia Article
* Drutopia Comment
* Drutopia Page

.. toctree::
   :maxdepth: 2
   :caption: Related Documentation

   development
   extension-criteria-and-candidates
   technical-guide
   new-content-feature
   extending-features
   development/add-user-permissions
