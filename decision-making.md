# Decision Making
Drutopia is a member-owned cooperative. All members have equal voting power to elect the Leadership Team, the ability to become a project maintainer, set the strategic goals of Drutopia and inform the technical roadmap.

## Membership

To become a member of Drutopia, a person must do to the following -
* Pay an annual membership due of $600 (or $50 a month)
* Agree to the Drutopia Code of Conduct


## Leadership Team

The Leadership Team can make strategic decisions regarding Drutopia. This includes vision, mission, goals, outreach strategy and target audiences, licensing. Decisions are made by consensus.


### Joining the Leadership Team

The current leadership team is:

* Ben
* Clayton
* Leslie
* Nedjo
* Robert
* Rosemary

We are currently looking for a representative for front-end development. Open positions will be filled by consensus from the current leadership team.

Once the project has been established for a longer time a democratic process driven by Drutopia members at large will be implemented.

### Leadership Team Elections

Elections of the Leadership Team will happen annually, once membership is open to the public.

## Working Groups

Working Groups make tactical decisions. Each working group has a working group lead. This lead also has the project maintainer role on the project. These are informed by the strategic decisions made by the leadership team. This includes approving pull requests, creating feature requests, determining technical and architectural approaches to the project.

All commits are made by creating a pull request. Any maintainer can approve a pull request. Any Drutopia member who has had a commit accepted to the project can become a maintainer if they'd like. If at any time a maintainer violates the Drutopia Code of Conduct their maintainer privileges will be suspended until maintainers and the leadership team resolve the issue.

The current working groups (and working group leads) are-
* Design
* Front-end development @nedjo
* Back-end development @nedjo
* Site Building @rosemarymann
* User Experience @cedewey
* Outreach/Marketing @mlncn

See <a href="/working-groups.html">working groups</a> for more detail.
