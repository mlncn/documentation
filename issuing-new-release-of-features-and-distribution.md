# Issuing a new release of the Drutopia base distribution

Here are the steps for issuing a new release of Drutopia's main distro.  Other Drutopia-based distributions should work similarly.

### Issue new feature module releases as needed

We need new releases of any feature that has had changes made.

* Ensure updates for all projects are pushed to GitLab, GitHub, and drupal.org (and are up-to-date on your local).  See the [readme](https://gitlab.com/drutopia/hackysync/blob/master/README.md) in the [hackysync](https://gitlab.com/drutopia/hackysync) repo.
* Determine which, if any, Drutopia feature project has changes since the last release:
    ```
    cd hackysync
    git pull
    ./get_all_repositories.sh
    ./update_drupal_org_github_from_gitlab.sh
    ./commits-since-most-recent-tag.sh
    ```
* For each Drutopia feature project with changes since the last release:
    * Use the hackysync script to create and push a new tag. Example: `./tag_project_release.sh drutopia_article 1.0-alpha1`.
    * Post a new release on drupal.org. See [relevant documentation](https://www.drupal.org/node/1068944).

### Document

* Update `drutopia/composer.json` requirements so that each Drutopia project references the latest release.  Get an overview of current releases with `./current-tags.sh`.  (This isn't necessary technically but is hugely useful in understanding what has changed between releases.)
* Paste the results of the automated check of changes since last stable release (from above, don't re-run) into https://gitlab.com/drutopia/documentation/edit/master/drutopia-releases.md 

### Test

* Install a fresh Drutopia site, ensuring it receives the newly posted release versions, and test.

### Issue a release of the Drutopia distribution

* Add an `8.x-1.0-alphaXX` tag and accompanying `1.0-alphaXX` tag and push them to GitLab, Github, and drupal.org repos. This can be done using the hackysync script: `./tag_project_release.sh drutopia 1.0-alphaXX distro`. Note that the third argument - in this case, "distro", though in fact it could be anything - is what triggers the script to add the short version tag `1.0-alphaXX`. This is needed when packaging the `drutopia` project but not when packaging regular feature-type projects such as `drutopia_article`.
* On Drupal.org, post a release for the Drutopia project.
