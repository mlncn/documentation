# ***EARLY DRAFT*** Drutopia Platform Member Guide

Drutopia is a project working to become a multi-stakeholder cooperative of software contributors, website managers and general supporters. Together we provide a Libre Software as a Service providing websites and other online communication & coordination tools to grassroots groups and other people with a purpose.

We are connected with all Drutopia projects by the [Drutopia Points of Unity](../points-of-unity.html).

You can join as an individual or a group. Group membership requires one point person. However, additional people in the organization can sign up as individual members if they'd like.


As a member of Drutopia, you have further benefits & responsibilities:

## Benefits

* A website (of course!)
* A test site
* Complete control over your site's theme
* Full ownership of the content you produce for your site
* An opportunity to suggest new features

## Responsibilities

* You agree to abide by the [Drutopia code of conduct](../drutopia-code-of-conduct.html)
* Payment of membership dues either monthly or yearly.

## Making Decisions

As a group intending to be a cooperative, we run according to the one member, one vote model. We strive for consensus when making decisions.

Any member can bring forth a proposal by submitting an issue to the [Drutopia repository](https://gitlab.com/drutopia/drutopia). Other members can then discuss the proposal.

## Building Your Site

We have an extensive user guide to answer any questions you have on how to build your site. We also encourage active exploration of Drutopia's features as a way of learning.

* [Read the User Guide](http://docs.drutopia.org/en/latest/end-user-documentation.html)

## Getting Help and Connecting with the Community

If you are ever stuck or have a question, you can reach out in our chat room at chat.drutopia.org or filing an issue in the Drutopia repository.

## Reporting Bugs

If you believe you've found a bug, reporting that is very helpful to the development team. You can file a bug by filing an issue at gitlab.com/drutopia/drutopia. There is a bug reporting template to walk you through the process.

## Conclusion

We're excited to have you part of Drutopia. If there is anything unclear or missing from this guide, let us know by filing an issue at gitlab.com/drutopia/documentation or sending an email to info@drutopia.org


