
```
mkdir -p ~/Projects/drutopia-platform/sites/your-new-site/
cd ~/Projects/drutopia-platform/sites/your-new-site/
cp -pr ~/Projects/drutopia/drutopia_vm/.gitignore .
cp -pr ~/Projects/drutopia/drutopia_vm/composer.json .
cp -pr ~/Projects/drutopia/drutopia_vm/Vagrantfile .
cp -pr ~/Projects/drutopia/drutopia_vm/box/ .
cp -pr ~/Projects/drutopia/drutopia_vm/provisioning/ .
v provisioning/box/config.yml
```

After editing `provisioning/box/config.yml` to replace `drutopia-vm` references with `your-new-site` instead, you're ready to bring up your new virtual machine for the first time:

```
vagrant up
```

It will take a while and you'll have to enter your administrator password in the middle.
