# Manifesto

Drutopia is an initiative to revolutionize the way we build online tools. We're combining the principles of software freedom, community ownership and intersectional politics to co-develop technologies that meet our needs and reflect our values.

Tools such as Squarespace and NationBuilder have lowered the cost for launching a website. However, these tools are limited in functionality and push people getting into web development for a cause into the dead end of proprietary services.

We intend to develop a framework upon which an ecosystem of open source distributions will flourish- meeting the unique needs of grassroots nonprofits, unions, food co-ops, small businesses and others making big change on small budgets. We will also use this framework to build a member-owned platform spanning multiple participating hosts. Members of the platform will drive the vision of the project including, crucially, guiding the development of new features.

We are looking for designers, authors, community organizers, media strategists, artists, developers, open-source enthusiasts, information architects, project managers, activists, and anyone passionate about community technology to join us.

If you support this initiative, add your voice to the statement at [drutopia.org](https://drutopia.org)
