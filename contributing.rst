Contributing
============

Hello and welcome! There's always a lot happening, but we always have time to help orient folks getting involved for the first time. The best way to get involved is to :doc:`join the conversation through video calls and text chat </communication>`.

It's also helpful to know how we work together. The :doc:`/drutopia-code-of-conduct` outlines how we center collaboration and respect with one another.

Some types of contributions we're set up to welcome!:

Bug Reports/Testing
^^^^^^^^^^^^^^^^^^^

When something is broken—preventing normal/typical use of Drutopia—please write a bug report.

How to report a bug
"""""""""""""""""""""""""""""""

Development
^^^^^^^^^^^

We welcome coders of all backgrounds and skillsets! Drutopia is a great place to build and learn.

To get started,

1. Review the :doc:`technical-guide`
2. :doc:`Join the conversation </communication>`
3. :doc:`Follow the development documentation </development>`

As soon as you are working on changes or additions to code, configuration, design, or documentation, create a new branch to commit your work to.

Merge requests are the primary mechanism we use to change Drutopia.  This allows you tell others about changes that you've pushed to your branch.

Once a merge request is sent, the rest of the team can review the set of changes, discuss potential modifications, and even push follow-up commits if necessary.  GitLab will correctly make merge requests against the main branch by default; for Drupal projects this is usually 8.x-1.x.

Design
^^^^^^

Much design, in particular user interface (UI) and user experience (UX) design are embodied in the fixes and features worked on in :doc:`development </development>` and we welcome design, usability, and accessibility review at all points in the development process; please get involved!

Ultimately we plan to have a vibrant selection of designs for people building sites on Drutopia to choose from.  Drutopia designs will be implemented as Drupal themes, using Twig templates, but these targeted use cases of Drutopia profiles will make it easier to produce a great-looking and great-working theme than the extremely expansive use cases a generic theme must support.

We also have need of designs to mock up better ways visitors and content editors can interact with existing features, and to prototype new features.

Use any of our :doc:`communication channels </communication>` to introduce yourself and a project member will work with you on getting started.

Documentation
^^^^^^^^^^^^^

We document the project through a `documentation repository on GitLab <https://gitlab.com/drutopia/documentation>`_.  Every page, like this one, has an "Edit on GitLab" link at the top; however, we strongly recommend you clone the repository and edit locally so you don't lose your edits in GitLab's unstable forms.  The format is either Markdown or `Sphinx-enhanced <http://www.sphinx-doc.org/en/stable/rest.html>`_ ReStructuredText; the latter is recommended.

We try to keep in mind `these tips for beginner-friendly documentation <https://datamade.us/blog/better-living-through-documentation>`_.  It's a high bar and there's lot's of work needed to get there.

We keep track of documentation needs by posting issues to the project repository. See `the list of outstanding documentation issues <https://gitlab.com/drutopia/documentation/issues?label_name%5B%5D=documentation>_` for places you can help!

Feature requests
^^^^^^^^^^^^^^^^

If you've got a great idea, we want to hear about it!  Before making a suggestion, here are a few handy tips on what to consider:

* `Search to see if the feature has already been requested <https://gitlab.com/drutopia/drutopia-distribution/issues?label_name%5B%5D=suggestion>`.
* TODO write a section like this?  Check out What makes it into Drutopia core? - this explains the guidelines for what fits into the scope and aims of the project
* Remember, your suggestion can't get adopted if people don't understand it. Please provide as much detail and context as possible. Explain the use case and why it is likely to be common. The strongest vote in favor of any feature request is clear demand among potential users/members of Drutopia.
* When making the request, tag it with "suggestion".  TODO create issue template for this.

Project management
^^^^^^^^^^^^^^^^^^

Do you have skills as a project manager or have interest in helping to plan, track and bring Drutopia features to completion?  Here are some of the things you can do to become involved in the project management aspect of the project:

* `Sign up as a supporter <https://drutopia.org>`_ and find out more about the project
* Join our core project management team. Contact leslie@drutopia.org for more info on this.
* We can use help:
  * Creating user stories for a Drutopia initiative of interest to you
  * Breaking out the Drutopia initiatives into a set of tasks that can be worked on by the initiative team
  * Checking in on the progress of these tasks to ensure that overall Drutopia project schedules are being met
  * Reporting the status of an initiative to the initiative team


Outreach
^^^^^^^^

Drutopia's public presence includes our web site, our projects on Gitlab and Drupal.org, social media, and (gasp) in-person gatherings at conferences and meetups. Here are just some of the ways you can help Drutopia:

* `Sign up as a supporter <http://drutopia.org/>`_
* Retweet and share Drutopia content
* Join our social media team to tweet and post to Facebook on behalf of Drutopia
* Respond to questions and comments on social media
* Plan and/or host hack-a-thons, trainings and presentations
* Blog about Drutopia!

Research
^^^^^^^^

A major focus of Drutopia is building well designed tools that meet users' needs. Join the collaborative research effort to ensure that grassroots organization's voices are heard.

* Interview grassroots organizations (we have questions to get your started)
* Share our survey at `drutopia.org/survey <https://drutopia.org/survey>`_ with people who would benefit from Drutopia
* Join our core research team to analyze our research data and make suggestions for the roadmap and enhancements to the platform. Contact clayton@drutopia.org for more info on this.


--------


.. toctree::
   :maxdepth: 1
   :caption: Contribution quick links

   reporting-bugs
   development
