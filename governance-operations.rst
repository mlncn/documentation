Draft Potential Governance and Operations Structure
===================================================

Overview
--------

Drutopia is a democratic project governed internally and in its operations according to sociocratic principles.  Democracy is the rule of the many, with the idea that if at least a bare majority approve of an action (even if that action is to elect a representative), it has some legitimacy.  Sociocracy, rule of the associated, embodies the idea that all people who must carry out a decision have to consent to the course of action.  Everyone gets a say in the direction and conditions of their work.  No one gets to say they are just following orders.

The structure is a bunch of circles, each linked to one or more other circles by a double link: a member of each circle delegated to represent their circle in the other circle.  Therefore, each linked circle has at least two people who are members of both circles.

All decisions made within circles are made by `consent <http://www.sociocracyforall.org/projects/consent-process/>`_, which is basically consensus but sociocracy chooses a different word to put extra emphasis on the fact that everyone in the circle is stating that the collective decision is one they can live with (not one they wholeheartedly endorse).  The decision-making process requires everyone involved be `heard from <http://www.sociocracyforall.org/projects/rounds/>`_, and the process encourages making decisions based on data and scheduling a time to revisit decisions.

Drutopia uses the more familiar term *team* rather than *circle* in describing its structure.

Any team can create another team for advising or carrying out tasks.  Most teams like this are open to anyone willing to take part in doing the work.  In this Drutopia's structure is similar to the structure common to many Free/Libre Open Source Software projects, `do-ocracy <http://data.agaric.com/drupal-do-ocracy>`_ (rule of those who do the work) with more accountability.

While working groups can be created and disbanded, keeping Drutopia's structure flexible to expand and adapt as needed, there's a core structure which we'll start out with initially, diagrammed below:

.. image:: images/governance-operations-circles.jpg


Drutopia Software Cooperative Teams
-----------------------------------

.. _guides:
Guides
^^^^^^

The guidance team is made up of the :ref:`elected representatives of the full membership <election-of-guides>`.  It fills the role of what in sociocracy is often called the 'top' circle.

Links
"""""
* :ref:`stewards`
* :ref:`community`
* :ref:`code`


.. _stewards:
Stewards
^^^^^^^^

The stewardship team is the central hub of all work that gets done for the cooperative.  It fills the role of what in sociocracy is often called the 'general' circle.  All, or nearly all, of its members will be in the stewardship team as a consequence of their roles as links to other teams.

Links
"""""
* :ref:`guides`
* :ref:`community`
* :ref:`code`
* :ref:`volunteer-care`
* :ref:`paid-worker-care`
* :ref:`security`
* :ref:`distributions`


.. _community:
Community
^^^^^^^^^

The community team is tasked with maintaining tools used by members of the Drutopia Software Cooperative when helping one another and writing code.

Links
"""""
* :ref:`guides`
* :ref:`stewards`
* :ref:`volunteer-care`

.. _code:
Code
^^^^

The code team, also called the technical working group, is responsible for approving which code gets into Drutopia core and, through linked teams, Drutopia-related distributions.

Links
"""""
* :ref:`guides`
* :ref:`stewards`
* :ref:`security`

.. _outreach:
Outreach & Onboarding
^^^^^^^^^^^^^^^^^^^^^

The outreach and onboarding team is charged primarily with bringing marginalized people into the Drutopia software cooperative, in particular into paid positions and positions of leadership.  This is part of Drutopia's commitment to equality (a requisite precondition to justice and liberty).  Specifically, the outreach and onboarding team is responsible for making and keeping Drutopia inclusive regarding gender, gender identity, sexual orientation, ethnicity, ability, age, religion, geography, and class.

Making and keeping Drutopia live its values of justice and equality goes much deeper than outreach and onboarding.  Outreach and onboarding, as responsible for being most in touch with many disadvantaged people, plays a role in helping all other teams improve their actions on this front.  Alternatively, if this is overloading one team, we should create an 'internal justice' or such team for this overriding goal.

  Rather than tech diversity and inclusion, we should instead seek tech equity and justice: explicitly political, less politically empty and mutable language. This is, of course, a far broader and more challenging goal. The explicit political, anti-oppressive nature of tech equity and justice will undoubtedly draw more fire from those committed to maintaining the existing hierarchies and inequities in tech. Additionally, tech equity and justice would need to address a far wider range of inequities and injustices connected to tech, from the racist gentrification of San Francisco and the East Bay, to exploitative labor practices required for the abundance of cheap electronics and devices we consume as techies, to the environmental and human destruction wreaked in order to obtain the basic materials for creating those devices.

  Yes, tech equity and justice is a much more challenging road for us to walk, but that is because it gets to the roots of the systems that have made tech diversity and inclusion efforts necessary in the first place. And when you get at the roots of a problem, you’re far more likely to get rid of the problem for good.

  -- Jack Aponte, "`The centre cannot hold: on diversity and inclusion in tech <http://jackalop.es/2017/08/07/centre-cannot-hold-diversity-inclusion-tech/>`_"

Secondarily, the members of the outreach team work to ensure that members of all teams are actively involved in organizations that fit the profile of organizations, of grassroots groups, that Drutopia exists to support.

Links
"""""
* :ref:`guides`
* :ref:`stewards`
* :ref:`paid-worker-care`
* :ref:`volunteer-care`

.. _volunteer-care:
Volunteer care
^^^^^^^^^^^^^^

Links
"""""
* :ref:`stewards`
* :ref:`paid-worker-care`
* :ref:`outreach`

.. _paid-worker-care:
Paid worker care
^^^^^^^^^^^^^^^^

Links
"""""
* :ref:`stewards`
* :ref:`volunteer-care`
* :ref:`outreach`

.. _security:
Security & Privacy
^^^^^^^^^^^^^^^^^^

The security and privacy team is responsible for oversight of security, the protection of personal information, privacy, and freedom from surveillance— both in the Drutopia codebase and Drutopia Software Cooperative's own application of Drutopia code and other software.

In addition, the security and privacy team works with the security teams of approved platforms to review security and privacy practices.

Links
"""""
* :ref:`stewards`
* :ref:`code`
* :ref:`community`

.. _distributions:
Distributions
^^^^^^^^^^^^^

The distributions team 

.. `saas`
SaaS

.. `_platform-coops`
Platform cooperatives
^^^^^^^^^^^^^^^^^^^^^

The platform cooperatives team is tasked with approving Drutopia-providing platform cooperatives and representing their interests to the Drutopia Software Cooperative.

#Question  Even if we had a separate circle approve platform coops, the platform coop circle would have to consent to a representative of an accepted platform coop joining the circle.  Drutopia wants to encourage cooperation not competition, but — matters of interpretation aside — representatives of platform coops already represented in this circle may have   .  The same thing might happen with seating representatives of the elected interest groups.

.. _election-of-guides:
Election of Guides
------------------

In the spirit of drawing on outside talent (advocated for in sociocracy), guides do not have to be Drutopia members.

Anyone looking to influence the cooperative through direct representation on the guidance team would form an "interest group" with at least four members.  While demographics does not determine policy, it is a highly visible indicator of inequality of power which we can directly take into account.  Therefore for an interest group of four, at least two people must be women or non-binary and at least two must be people of color (these can of course be the same people, and it still counts).  For larger interest groups the proportion of each must be 40 percent, rounded to the nearest whole human.

Each interest group can state its priorities and reasons for seeking representation on the guidance team, and the full membership votes for their groups they think will provide the best guidance, ranked choice style.  Any group with at least ten percent support among the membership can have a representative on the guidance team (pending consent of the guidance team to that particular person?)

Interest group representation on the guidance team may be single-linked, rather than the more common double link, because there's not expected to be a dedicated communication channel from the guidance team to interest groups, just from interest groups to the guidance team.

Equal access to communication is crucial to equal access to decision-making power.  To that end, any interest group or individual member who has a message they believe to be important enough to go to the full membership will receive review of the message by a dozen other members, and if they concur the message will go out to the full membership.  This allows moderation to prevent an overwhelming number of messages or unwanted messages, but keeps the moderation in the democratic control of the full membership.

Drutopia Platform Cooperative Teams
-----------------------------------

The first thing to point out about the platform cooperative is that the single Drutopia Software Cooperative is set up to work with any number of platform cooperatives, or indeed other hosting providers which may not be organized as platform cooperatives but still meet the 

