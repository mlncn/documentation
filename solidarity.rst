Solidarity
==========

Solidarity is an online `platform cooperative <https://www.wikiwand.com/en/Platform_cooperative>`_ designed by grassroots organizers to help one another act, mobilize and support one another.

The platform is built around four main features: groups, campaigns, actions and news.

`View the Solidarity Prototype <https://marvelapp.com/27hgh3d/screen/28546626>`_

Personas
--------

See :doc:`solidarity-personas`

Groups
------

Each group has a page within the website. A group can launch a campaign, call for an action and publish a news article. All of these pieces of content are displayed on a group’s page, creating a mini site.

Potential Future Features
^^^^^^^^^^^^^^^^^^^^^^^^^

* Group membership
* Group only content
* Integration with other tools such as: Discourse, Facebook, Loomio, Slack, Twitter

Campaigns
---------

A campaign page is a central place to explain the issue, publish news articles about its activity, list out demands, post calls to action and raise funds.

Potential Future Features
^^^^^^^^^^^^^^^^^^^^^^^^^

* Onsite Petition

Action
------

An action is a call for concrete work from supporters. It can stand on its own or be part of a campaign. It can have a goal and due date set,. For example, a call for 50 people to call a prison by June 13th demanding an investigation into prisoner abuse. An action can also be turned into a fundraiser, allowing people to donate directly to the site through Stripe. No longer will groups be dependent on proprietary platforms such as GoFundMe and IndieGoGo.

Potential Future Features
^^^^^^^^^^^^^^^^^^^^^^^^^
* SMS integration
* Badges and other game design features

News
----

News articles on Solidarity contain depth and functionality beyond what is found on a typical site. An article can be part of a larger campaign or associated with specific groups.  By relating an action to the article, the post now becomes a mobilizing opportunity for readers. An article can also be posted automatically to social media accounts including: Facebook, Mastodon, and Twitter.

Potential Future Features
^^^^^^^^^^^^^^^^^^^^^^^^^

* Collaborative, moderated editing of an article (think wiki meets blog)
* SMS integration

Additional Features
-------------------

Solidarity also boasts a customizable homepage, user management, content moderation tools and all of the extendability that comes with Drupal. Because it is open-source, it will always be free to download. Your data is yours and well structured to export to another platform at any time.

Future Features
---------------

We believe in constant improvement and iteration. Future features will be driven by the needs and feedback of the Solidarity community. Some of the additional features being talked about right now are:

* Additional themes
* Customizable page layouts and templates
* Events
* Resources
* More sophisticated fundraising
* Constituent Relationship Management integration
* Mobile app for your site

