=================
Drutopia Partners
=================

A great way to serve grassroots organizations is by becoming a Drutopia Partner. Partnership is available to both agencies and freelancers.

As a Drutopia Partner you and your client(s) receives

* membership to the Drutopia Software Cooperative
* a local, test and live environment with continuous integration for each client site
* membership to MayFirst/PeopleLink, which includes the following for each client site:
  * Web hosting
  * Email
  * Email lists
  * NextCloud accounts
  * Discourse accounts
* same day Drupal and Drutopia updates applied by the Drutopia team
* a Git repository with full control over your client's theme and certain configuration settings
* Support forum to ask questions and report bugs.
* Public recognition on drutopia.org

To become a Drutopia Partner,

* read and agree to the :doc:`Points of Unity <goals.html#points-of-unity>` and :doc:`Code of Conduct <drutopia-code-of-conduct.html>`
* sign up each of your :doc:`Platform Cooperative <platform-cooperative.html>` clients up
* commit to supporting the client's theme and configuration
* pay Drutopia Platform Cooperative membership dues on time
* contribute improvements back to the Drutopia project when possible
* serve as a helpful intermediary between your clients and the Drutopia project

