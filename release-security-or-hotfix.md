# Release hotfix or security update to a Drutopia distribution

When issuing a hotfix release (including security) do not include any unrelated changes.

Typically this is simple: when Drupal or an underlying library receives an update, the Drutopia installation profile
