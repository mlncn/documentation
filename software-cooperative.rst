=============================
Drutopia Software Cooperative
=============================

The Drutopia project is governed cooperatively, with a leadership team providing strategic guidance, technical leads deciding technical decisions and members driving forward the roadmap.

Membership
----------

Membership is open to all, simply -

* Pay an annual membership due of $10-100 ($50 recommended) or one time lifetime membership of $100
* Agree to the :doc:`Drutopia Code of Conduct >drutopia-code-of-conduct.html>` and :doc:`Points of Unity <goals.html#points-of-unity>`

Members are encouraged to request and vote up/down features, report bugs and contribute to the project.

Leadership Team
---------------

The Leadership Team makes strategic decisions regarding Drutopia. This includes vision, mission, goals, outreach strategy and target audiences, and licensing. Decisions are made by consensus.

Joining the Leadership Team

The current leadership team is:

* Ben
* Clayton
* Leslie
* Nedjo
* Rosemary

We are currently looking for a representative for front-end development. Open positions will be filled by consensus from the current leadership team.

Once the project has been established for a longer time a democratic process driven by Drutopia members at large will be implemented.

Leadership Team Elections
=========================

Elections of the Leadership Team will happen annually, with our first election to be held November 2018. Each member gets on vote.

Technical Leads
---------------

Technical leads make the final call on technical decisions.

Current technical leads are:

* <a href="https://gitlab.com/mlncn">Ben</a>
* <a href="https://gitlab.com/cedewey">Clayton</a>
* <a href =https://gitlab.com/nedjo">Nedjo</a>
* <a href="https://gitlab.com/rosemarymann">Rosemary</a>


Any member is welcome to request becoming a technical lead by

* asking a current tech lead to become one
* a current tech lead then nominates them
* One other tech lead must approve the person
* Once approved, that person's gitlab account is elevated to the Owner role
