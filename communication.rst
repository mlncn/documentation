Getting Plugged In
==================

We encourage members, supporters, and contributors to join the conversation through video calls and Mattermost conversations too.  Always keep in mind the :doc:`/drutopia-code-of-conduct`.

General meetings
----------------

During the summer of 2019 we have general meetings every other Tuesday from 2 pm–2:30 pm Eastern Time (June 11, June 25, July 9, July 23, August 6, August 20, September 3).

Newcomers and anyone who feels they need to catch up with what's going on a bit are invited to join.

You can join us using Zoom:

https://zoom.us/j/695543337

Or by telephone:

    | In US dial: +1 646 558 8656  
    | International numbers available: https://zoom.us/zoomconference?m=l7EUL8Ml2IXLmCDn-W-y0Rzd_wz8Dzq1
    | Meeting ID: 695 543 337

Additional times to talk will be scheduled as needed, so if you have something to discuss, please be in touch!

Chat.Drutopia.org
-----------------

We use the Mattermost messaging app to discuss ideas and get help from each other. Mattermost is a free and open source chat system that is similar to the proprietary Slack.

Chat through the web browser at https://chat.drutopia.org

Or install the Mattermost app for your device https://about.mattermost.com/download/

Issue queues
------------

The many projects that make up the Drutopia initiative are in our GitLab group:
http://gitlab.com/drutopia/

Post questions or feedback or requests to the queue of the relevant project; if you're not sure you can always use the main Drutopia distribution issue queue: https://gitlab.com/drutopia/drutopia/issues/

Try searching the issues first: https://gitlab.com/groups/drutopia/issues

E-mail
------

If you have any questions or trouble getting set up contact us at info@drutopia.org and we'd be happy to help.

