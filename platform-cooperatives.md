# Platform Cooperatives

Drutopia is built to support the proliferation of [platform cooperatives](https://en.wikipedia.org/wiki/Platform_cooperative), online platforms owned collectively by its members.

The first of these platform cooperatives is  designed by grassroots organizers to help one another act, mobilize and support one another.

If you are interested in joining an existing platform coop or starting your own using the Drutopia architecture, contact the Drutopia leadership team at info@drutopia.org

## Solidarity Libre Software as a Service

A hosted option is available for $50/month which provides the following:

* Platform Cooperative membership
* Security updates
* Email aliases
* NextCloud (and other MayFirst services)
* Hosting Support
* Training & Documentation
* Mattermost Support Channel
* One hour "ask anything" each month


