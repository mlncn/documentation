# Drutopia releases complete changelog

Each release listing includes the commits to all constituent Drutopia projects— the feature modules and themes that make up the Drutopia base distribution.  It is meant primarily as an aid for writing more human-friendly updates about what has changed in a release.

```eval_rst
.. note::
   Adding information here is a `manual process`, so we may be missing releases.  
```
## 1.0-beta2
### drutopia_action
[78fe14e](https://gitlab.com/drutopia/drutopia_action/commit/78fe14ea52789ebc7c664e150ac42b44d2f1ceff) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[3629ed4](https://gitlab.com/drutopia/drutopia_action/commit/3629ed4072169e1fff1372acb1dfd16cda588e75) Update parent menu item (Rosemary Mann)  
[1a3cd39](https://gitlab.com/drutopia/drutopia_action/commit/1a3cd3956c8ccbac51ec9a756fee4e954be72ea2) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[1feea18](https://gitlab.com/drutopia/drutopia_action/commit/1feea18d4bc8c352901b092770711d277f9507d5) Update to use focal point widget (Rosemary Mann)  
[fb6ff45](https://gitlab.com/drutopia/drutopia_action/commit/fb6ff45208ab7ec33d48dcb58bad438cc057ab0f) Merge branch '24-display' into '8.x-1.x' (Nedjo Rogers)  
[feffa4b](https://gitlab.com/drutopia/drutopia_action/commit/feffa4bbf1ab9366a12bb11ad80e8c60b2e43c95) Issue #24: Disable css (Rosemary Mann)  
### drutopia_article
[192c7db](https://gitlab.com/drutopia/drutopia_article/commit/192c7db42ce5bc4ad0c936fca22a6b2b8e1909e8) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[5118b7b](https://gitlab.com/drutopia/drutopia_article/commit/5118b7b0247be8d0f066b1e69431dc4bdf499818) Update parent menu item and title (Rosemary Mann)  
[a5f92a1](https://gitlab.com/drutopia/drutopia_article/commit/a5f92a19f7740efdd2bdffef500083b251439615) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[76194c5](https://gitlab.com/drutopia/drutopia_article/commit/76194c5af321a8607d42f93e8dab955a80a23e85) Update to use focal point widget (Rosemary Mann)  
[484122d](https://gitlab.com/drutopia/drutopia_article/commit/484122dce4b417c9647a90f3b1f12ac9b7f160e6) Merge branch '28-search' into '8.x-1.x' (Nedjo Rogers)  
[f17a54d](https://gitlab.com/drutopia/drutopia_article/commit/f17a54df7a63306b23525e0f53a6d3f105fc14a6) Issue #28: Configure search index view mode (Rosemary Mann)  
[5324e62](https://gitlab.com/drutopia/drutopia_article/commit/5324e62a8d2e5327aa473c5e7ab6d7ac7f70fdaa) Merge branch '31-display' into '8.x-1.x' (Nedjo Rogers)  
[5a13092](https://gitlab.com/drutopia/drutopia_article/commit/5a130921b82f0823fb7e19adff33250efefe31ce) Issue #31: Disable css (Rosemary Mann)  
### drutopia_blog
[e682400](https://gitlab.com/drutopia/drutopia_blog/commit/e682400080fce5a5d63ce47d715e69ec5bb1b7e1) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[d4b2977](https://gitlab.com/drutopia/drutopia_blog/commit/d4b29773fa0ddfd682f755a523958eee97dbc995) Update parent menu item (Rosemary Mann)  
[52c6361](https://gitlab.com/drutopia/drutopia_blog/commit/52c636160f2fd35f9938ed07a574610edbdf8263) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[e01b80d](https://gitlab.com/drutopia/drutopia_blog/commit/e01b80dde0ce99470a366e44a3970c6483be8e5c) Update to use focal point widget (Rosemary Mann)  
[d05cb48](https://gitlab.com/drutopia/drutopia_blog/commit/d05cb48a249556457d9cae4cc75dc38eaf653bd2) Merge branch '23-display' into '8.x-1.x' (Nedjo Rogers)  
[171095d](https://gitlab.com/drutopia/drutopia_blog/commit/171095dea18735d0dcd82b33c666ef8ca8a17e7c) Issue #23: Disable css (Rosemary Mann)  
### drutopia_campaign
[42c0e0c](https://gitlab.com/drutopia/drutopia_campaign/commit/42c0e0cdd920e8b1aaff5a82a48599c1c828dabc) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[8806197](https://gitlab.com/drutopia/drutopia_campaign/commit/8806197a2c6bcfa2a56522eceacb359bc96ea5a2) Update parent menu item (Rosemary Mann)  
[3c31260](https://gitlab.com/drutopia/drutopia_campaign/commit/3c31260d8fd778a0913c62496d9451f87c98b2fe) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[6b17b2e](https://gitlab.com/drutopia/drutopia_campaign/commit/6b17b2e13a52a9f03535d9f999dfa2c10ce8dfc4) Update to use focal point widget (Rosemary Mann)  
[62d8fa0](https://gitlab.com/drutopia/drutopia_campaign/commit/62d8fa014fbb433f3a97476303fffca11dabc7c5) Merge branch '10-display' into '8.x-1.x' (Nedjo Rogers)  
[8323cdb](https://gitlab.com/drutopia/drutopia_campaign/commit/8323cdb443385d9aff3258e432d2afaf36509c9b) Configure teaser as two column and disable css (Rosemary Mann)  
### drutopia_core
[234352c](https://gitlab.com/drutopia/drutopia_core/commit/234352cd66aa256b66730d5f53ee992889b15eef) Merge branch '31-dependency' into '8.x-1.x' (Nedjo Rogers)  
[669ddd7](https://gitlab.com/drutopia/drutopia_core/commit/669ddd759a11bfadf8888daed435dd50cb3ab81a) Issue #31: add dependency on Default Content module (Rosemary Mann)  
[1760f9f](https://gitlab.com/drutopia/drutopia_core/commit/1760f9f917aed8a069cb0d63eeab641451463001) Merge branch '31-menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[ae3180b](https://gitlab.com/drutopia/drutopia_core/commit/ae3180b11385fd3db29630f5a4ef2d1a29e371a3) Issue #31: Add top level menu items as default content (Rosemary Mann)  
[059952b](https://gitlab.com/drutopia/drutopia_core/commit/059952ba1f4e732f6657066efc10848df142b792) Merge branch '33-responsive' into '8.x-1.x' (Nedjo Rogers)  
[fb9bb65](https://gitlab.com/drutopia/drutopia_core/commit/fb9bb65e80c6888e8565f50d63a2f7dbff836a87) Issue #33: Add new image styles and responsive image styles (Rosemary Mann)  
[917bf1a](https://gitlab.com/drutopia/drutopia_core/commit/917bf1ae2185fba308498be054c41acf17c94532) Merge branch '32-crop' into '8.x-1.x' (Nedjo Rogers)  
[3b8a99d](https://gitlab.com/drutopia/drutopia_core/commit/3b8a99de39d20e2c8be3bdc326b927c437de5e37) Tweak ordering of modules in update (Rosemary Mann)  
[2b4463d](https://gitlab.com/drutopia/drutopia_core/commit/2b4463ddf474ffffdf7cdb7ccbb6986e3e9c9c4d) Issue #32: Add Focal Point and Crop API modules (Rosemary Mann)  
[41380cd](https://gitlab.com/drutopia/drutopia_core/commit/41380cd5fada696f2eb19fff5dfb04f491bfd768) Merge branch '30-empty-facets' into '8.x-1.x' (Rosemary Mann)  
[6680f54](https://gitlab.com/drutopia/drutopia_core/commit/6680f54d5f569ce4dcee4158f1c08b480b9512b0) Issue #30: add patch on facets module to hide empty facet blocks (Nedjo Rogers)  
### drutopia_event
[cd2cd9f](https://gitlab.com/drutopia/drutopia_event/commit/cd2cd9f34a4ee8273fce882ab3612f7062bfcc54) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[d32e1d8](https://gitlab.com/drutopia/drutopia_event/commit/d32e1d8fe69cc7904300aa03cef9294d20f6ec7e) Update parent menu item (Rosemary Mann)  
[690c996](https://gitlab.com/drutopia/drutopia_event/commit/690c996b31356474b5445dcf697862eac10c919a) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[ff7c874](https://gitlab.com/drutopia/drutopia_event/commit/ff7c87425b3ec241da5da2e8578949c1b4a3c5d6) Update to use focal point widget (Rosemary Mann)  
### drutopia_fundraising
[fca7850](https://gitlab.com/drutopia/drutopia_fundraising/commit/fca7850cc16aa8279727b25b216879cf45a7efb1) Use new style of declaring dependencies (benjamin melançon)  
[e54576a](https://gitlab.com/drutopia/drutopia_fundraising/commit/e54576af783b00324233f895888b0cb979db48ab) Update minimum versions of dependencies (benjamin melançon)  
### drutopia
[6d08e07](https://gitlab.com/drutopia/drutopia/commit/6d08e0712cc63f03ea884b367da2ae745e34800d) Merge branch '265-upgradee-to-core-8-7' into '8.x-1.x' (Rosemary Mann)  
[1f38ad9](https://gitlab.com/drutopia/drutopia/commit/1f38ad978bd326f932f3b0068310eb53a97e794a) Issue #265: update to Drupal core 8.7.2 (Nedjo Rogers)  
[7ddb6c9](https://gitlab.com/drutopia/drutopia/commit/7ddb6c93ee35df89b1a83a64df4e1bc7ebaf3304) Merge branch '266-module-versions' into '8.x-1.x' (Nedjo Rogers)  
[a8225d7](https://gitlab.com/drutopia/drutopia/commit/a8225d77461688f9e150ab81fffaef80baa3c764) Issue #266: Update module versions as housekeeping (Rosemary Mann)  
[63444f7](https://gitlab.com/drutopia/drutopia/commit/63444f7a454a61ca8dab5a687500afa5b90aa0ac) Merge branch 'move-search-form' into '8.x-1.x' (Rosemary Mann)  
[40a4570](https://gitlab.com/drutopia/drutopia/commit/40a4570a0a842694c4048e137af9daa8d64c0e79) Move search form block to content region (Nedjo Rogers)  
[45e1c1b](https://gitlab.com/drutopia/drutopia/commit/45e1c1bccfa4322dd01bc5531eaafed8d4f5fe28) Merge branch '264-social-media' into '8.x-1.x' (Nedjo Rogers)  
[df31635](https://gitlab.com/drutopia/drutopia/commit/df3163578fc61508067302e618df924c994a7f54) Issue #264: Move social media block (Rosemary Mann)  
[fbf526b](https://gitlab.com/drutopia/drutopia/commit/fbf526b32e9872e1103ccea996b6b56de7eb163f) Merge branch '264-optional' into '8.x-1.x' (Nedjo Rogers)  
[5597523](https://gitlab.com/drutopia/drutopia/commit/5597523ff35fca86038f389fe2c0a2175765b5c5) Issue #264: Move new footer blocks to optional (Rosemary Mann)  
[b333bb6](https://gitlab.com/drutopia/drutopia/commit/b333bb6fd44e471e4b89445124c1198ff4a3ad4a) Merge branch '264-footer-blocks' into '8.x-1.x' (Nedjo Rogers)  
[e5aeefb](https://gitlab.com/drutopia/drutopia/commit/e5aeefb62936230111506072e53fff551fcea8d1) Issue #264: Add and update footer blocks (Rosemary Mann)  
[7453ce9](https://gitlab.com/drutopia/drutopia/commit/7453ce9e1f61e55f047c97cdd442111befdec12b) Merge branch 'bundle' into '8.x-1.x' (Nedjo Rogers)  
[f9e6103](https://gitlab.com/drutopia/drutopia/commit/f9e61030fc8c34f86cf61a2d7da6d1ce7349da50) Merge branch 'author-content' into '8.x-1.x' (Nedjo Rogers)  
[907e2c2](https://gitlab.com/drutopia/drutopia/commit/907e2c2903d71c800312ef38c76a89032c252272) Add content by author block to optional folder (Rosemary Mann)  
[eddb8fe](https://gitlab.com/drutopia/drutopia/commit/eddb8feb6e5c528ef3dd025d538d23233c05f5c9) Remove Drutopia bundle (Rosemary Mann)  
[efdf757](https://gitlab.com/drutopia/drutopia/commit/efdf757b504d5a5dc76d920f4bcf848873c3e450) Merge branch 'search-facets' into '8.x-1.x' (Nedjo Rogers)  
[863326e](https://gitlab.com/drutopia/drutopia/commit/863326e1cde5fcb24b9776a0b5be9d19bf4f2c43) Merge branch '262-remove-global-search-block' into '8.x-1.x' (Rosemary Mann)  
[6079327](https://gitlab.com/drutopia/drutopia/commit/60793271dbfaade3a1dd83df455e52c0c5169e8c) Add searcg facet blocks to optional folder (Rosemary Mann)  
[8cadf44](https://gitlab.com/drutopia/drutopia/commit/8cadf44d2752603fa14dff8375bcf1edd4b0b7c3) Issue #262: remove global search block (Nedjo Rogers)  
[cb2ff1f](https://gitlab.com/drutopia/drutopia/commit/cb2ff1f05f982431ab4a2145c26181e828958ee9) Merge branch '225-home-page-setting' into '8.x-1.x' (Rosemary Mann)  
[b114678](https://gitlab.com/drutopia/drutopia/commit/b114678fa8f934b08a95c59bbfb5a83617c58d59) Issue #225: fix node path (Nedjo Rogers)  
[1a451cc](https://gitlab.com/drutopia/drutopia/commit/1a451cc671a3a85beb9aa9e9a695fd3d385d087b) Issue #225: set the site front page to a system path rather an alias (Nedjo Rogers)  
[1f24c61](https://gitlab.com/drutopia/drutopia/commit/1f24c618bfcf820d78816ef7607868536f77bf48) Use empty array for paragraph behavior settings that won't blow up default content (benjamin melançon)  
### drutopia_group
[a92a7ad](https://gitlab.com/drutopia/drutopia_group/commit/a92a7ad3490d85f9c583443d0e2ac20056fce428) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[f5ffc62](https://gitlab.com/drutopia/drutopia_group/commit/f5ffc62314040aefde862dc0437203cdacc88867) Update parent menu item (Rosemary Mann)  
### drutopia_page
[fc39d22](https://gitlab.com/drutopia/drutopia_page/commit/fc39d22a0945dadb34521dc4602b74cb34b94493) Merge branch '15-default-content' into '8.x-1.x' (Nedjo Rogers)  
[0901e20](https://gitlab.com/drutopia/drutopia_page/commit/0901e2001690beeeda52de5ac6af8e692888479c) Issue #15: Update default content for privacy policy (Rosemary Mann)  
[a7709a8](https://gitlab.com/drutopia/drutopia_page/commit/a7709a8a6396f288132f5d0d73a9c151f12721f5) Merge branch '15-privacy' into '8.x-1.x' (Nedjo Rogers)  
[404ac35](https://gitlab.com/drutopia/drutopia_page/commit/404ac35f01a932a7959bfffec5cf8af5b7298787) Add default content for privacy policy (Rosemary Mann)  
[d648c0d](https://gitlab.com/drutopia/drutopia_page/commit/d648c0dc532a1113d6b58772042af8f3b6ed2548) Merge branch '31-about' into '8.x-1.x' (Nedjo Rogers)  
[c4e53a7](https://gitlab.com/drutopia/drutopia_page/commit/c4e53a74b69b7005c0bebf4c2009ea7999b744b2) Update default content for about menu link (Rosemary Mann)  
### drutopia_people
[c9c0df9](https://gitlab.com/drutopia/drutopia_people/commit/c9c0df914c8ffbc421773c586783344a7f4051f1) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[a11c13e](https://gitlab.com/drutopia/drutopia_people/commit/a11c13ea3004d00d61e068965e2e032206a9103e) Add menu item and parent (Rosemary Mann)  
[d41d87b](https://gitlab.com/drutopia/drutopia_people/commit/d41d87b9ec58cc27a17a0a554b5fc00a57ffc617) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[30cc34d](https://gitlab.com/drutopia/drutopia_people/commit/30cc34d61d66db012686e27e3a187789910b72ee) Update form to use focal point wdiget and update styles to use new tall and short responsive image styles (Rosemary Mann)  
[108cc42](https://gitlab.com/drutopia/drutopia_people/commit/108cc42028261c19319e7703ad09f4803c3ceda9) Merge branch '11-tweak' into '8.x-1.x' (Nedjo Rogers)  
[221d03f](https://gitlab.com/drutopia/drutopia_people/commit/221d03f0236915193d3e3b74633d202ccdecf542)  Issue #11: tweak view to hide if empty (Rosemary Mann)  
[1524e06](https://gitlab.com/drutopia/drutopia_people/commit/1524e0690fc9b27794b4ad77ccbb4024e3a0bcce) Merge branch '11-author' into '8.x-1.x' (Nedjo Rogers)  
[50f4b43](https://gitlab.com/drutopia/drutopia_people/commit/50f4b432b7054aad9ad8822ffa62cca8d41259c4) Issue #11: Add content by author view (Rosemary Mann)  
### drutopia_related_content
[72dcf2d](https://gitlab.com/drutopia/drutopia_related_content/commit/72dcf2d1d313a80fd2d65171f6ee3800d2e05fc5) Merge branch '2-people' into '8.x-1.x' (Nedjo Rogers)  
[6b4468a](https://gitlab.com/drutopia/drutopia_related_content/commit/6b4468ae20dadaefa52476580a6d544a362b34a0) Issue #2: Remove people and landing page content types from related content (Rosemary Mann)  
### drutopia_resource
[564ce4f](https://gitlab.com/drutopia/drutopia_resource/commit/564ce4f0f3cde680105e6e24252a3012dee935df) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[7a6cc88](https://gitlab.com/drutopia/drutopia_resource/commit/7a6cc8810e198f5e2b502162f95a8f567918a686) Update menu parent item (Rosemary Mann)  
[cf17528](https://gitlab.com/drutopia/drutopia_resource/commit/cf175282eb0545eabfe9b8f9aa760fd2f3b9738b) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[96a0dc1](https://gitlab.com/drutopia/drutopia_resource/commit/96a0dc1f18c1899792f5c3b22a2935402941b97f) Update to use focal point widget and update image style used for video field display (Rosemary Mann)  
### drutopia_search
[b0307b7](https://gitlab.com/drutopia/drutopia_search/commit/b0307b7f154ef1e5d19940bfb0d479715c8ba4cf) Merge branch '14-caching' into '8.x-1.x' (Nedjo Rogers)  
[7f4c6bb](https://gitlab.com/drutopia/drutopia_search/commit/7f4c6bb9b45b6f182ec89029626fcad6f25773ca) Issue #14: Set caching on view to none (Rosemary Mann)  
[fdf4c29](https://gitlab.com/drutopia/drutopia_search/commit/fdf4c29670ba8a03d92ecd2858b18c3bdd5c9fd4) Merge branch '11-search-link' into '8.x-1.x' (Rosemary Mann)  
[f20ada4](https://gitlab.com/drutopia/drutopia_search/commit/f20ada4c0c968f23c02348aff0a0d4be8497209b) Issue #11: add search menu link (Nedjo Rogers)  
[bacf431](https://gitlab.com/drutopia/drutopia_search/commit/bacf431edd96656b722f216cb2c6474be46a7c55) Merge branch '13-facets' into '8.x-1.x' (Nedjo Rogers)  
[8a0c303](https://gitlab.com/drutopia/drutopia_search/commit/8a0c303150772e9f65ba5b40e686f3282c9e22b2) Issue #13: Add facets to search results page (Rosemary Mann)  
[58eeaef](https://gitlab.com/drutopia/drutopia_search/commit/58eeaef1eb2e449a37ab02c9b8afd85f461f281f) Merge branch '10-views-row' into '8.x-1.x' (Nedjo Rogers)  
[5ba3799](https://gitlab.com/drutopia/drutopia_search/commit/5ba3799d32aa53b7da64bf6fc443900641452b09) Issue #10: uncheck add views row classes (Rosemary Mann)  
### drutopia_seo
[837848d](https://gitlab.com/drutopia/drutopia_seo/commit/837848d70e6276693e9b8b4523f32452719e86f0) Merge branch '12-redirect' into '8.x-1.x' (Nedjo Rogers)  
[148baed](https://gitlab.com/drutopia/drutopia_seo/commit/148baede4538e8563d7109161c5ad5e0908eba42) Issue #12: Add Redirect module including permissions and update (Rosemary Mann)  
### drutopia_storyline
[f8ebde4](https://gitlab.com/drutopia/drutopia_storyline/commit/f8ebde43a4dba9b19445d3e03132abb427a439da) Update dependencies just for appearances (benjamin melançon)  ### drutopia_user
### octavia
[ea28bba](https://gitlab.com/drutopia/octavia/commit/ea28bbaa44e611e2b098f20bc2912c5253da9919) Issue #57: adjust page title size at smaller breakpoints (Nedjo Rogers)  
[659b3fa](https://gitlab.com/drutopia/octavia/commit/659b3fa96d8f3b93b58366223435e1f8c35af1ba) Merge branch '59-logo-size' into '8.x-1.x' (Rosemary Mann)  
[868a1a9](https://gitlab.com/drutopia/octavia/commit/868a1a9f429d092bf83688e25bd408cb64c25373) Merge branch '64-address-regressions' into '8.x-1.x' (Rosemary Mann)  
[796f498](https://gitlab.com/drutopia/octavia/commit/796f498467059299c4238dfa8bcb14233d6c235f) Issue #59: increase logo size (Nedjo Rogers)  
[6d24a9a](https://gitlab.com/drutopia/octavia/commit/6d24a9a8e0e4fc4db516ac6fd0cf76a7f9302367) Issue #64: Address block theming regressions (Nedjo Rogers)  
[dc1492f](https://gitlab.com/drutopia/octavia/commit/dc1492f59dc72fab56ee8afe9e06bff18c29fc42) Merge branch '65-move-search-block' into '8.x-1.x' (Rosemary Mann)  
[78282e3](https://gitlab.com/drutopia/octavia/commit/78282e3246803ba8072c1338e6c5d7095adb6d03) Issue #65: move search block to content region (Nedjo Rogers)  
[5989452](https://gitlab.com/drutopia/octavia/commit/59894522ffcee83543543ac7648f921dcf7a8a5a) Merge branch '57-header-refresh' into '8.x-1.x' (Rosemary Mann)  
[afcaf03](https://gitlab.com/drutopia/octavia/commit/afcaf031bf5c443c6a10139884b9c4e10b4b0fca) Issue #57: fix add is-1 class to page title (Nedjo Rogers)  
[68e0ba7](https://gitlab.com/drutopia/octavia/commit/68e0ba7bb6039d4f7e0e70344ff57679dddae886) Issue #57: adjust local task menu style (Nedjo Rogers)  
[9c18d87](https://gitlab.com/drutopia/octavia/commit/9c18d87958052ac578bc8f9e3088bbad44ef4f7e) Issue #57: add is-1 class to page title (Nedjo Rogers)  
[e735a63](https://gitlab.com/drutopia/octavia/commit/e735a63b4607addfeadb7e5bce6e2470f70e11a5) Issue #57: remove bg colour from header (Nedjo Rogers)  
[6051890](https://gitlab.com/drutopia/octavia/commit/60518908127129f3151f618baba0bf83e084a492) Merge branch 'account-menu' into '8.x-1.x' (Nedjo Rogers)  
[8413f6f](https://gitlab.com/drutopia/octavia/commit/8413f6f181854e1c9ae27a59af9da4d9886c3ff4) Move user account menu block to new region (Rosemary Mann)  
[b4625d1](https://gitlab.com/drutopia/octavia/commit/b4625d1a4a871de9ff6e592fa2d530d729fdbdad) Merge branch '57-refresh-header' into '8.x-1.x' (Rosemary Mann)  
[6b918f4](https://gitlab.com/drutopia/octavia/commit/6b918f44f71b3e0f6463adbddaa6330917a49b4f) Issue #57: add region and handling for user account menu (Nedjo Rogers)  
[ef21e0f](https://gitlab.com/drutopia/octavia/commit/ef21e0f1fdbbf6e7130e08c40dd83b979b63b114) Merge branch '61-ds-tweaks' into '8.x-1.x' (Nedjo Rogers)  
[cd5e84f](https://gitlab.com/drutopia/octavia/commit/cd5e84fd0532f660938b4870e16d4b2ce34e7641) Issue #61: Updates to ds templates after image work (Rosemary Mann)  
[d68b29b](https://gitlab.com/drutopia/octavia/commit/d68b29b44d952352969ca117d55e9f7fec81e069) Merge branch '58-refresh-footer-region' into '8.x-1.x' (Rosemary Mann)  
[fd94245](https://gitlab.com/drutopia/octavia/commit/fd942458ccbcf31e2a092f0bedbd1c14c14734e2) Issue #58: refresh footer region (Nedjo Rogers)  
[097df5f](https://gitlab.com/drutopia/octavia/commit/097df5f77d4ed02bf4d70b721621af8e4b6ee786) Merge branch '11-search-link' into '8.x-1.x' (Rosemary Mann)  
[9240594](https://gitlab.com/drutopia/octavia/commit/92405946430b84c01d3838fa3c820562a6075d55) Issue #11: convert the search menu item title to an icon (Nedjo Rogers)  
[47f7df1](https://gitlab.com/drutopia/octavia/commit/47f7df157b158a47a9c226490537015a1c3343a4) Merge branch '56-columns-spacing' into '8.x-1.x' (Rosemary Mann)  
[c1b01be](https://gitlab.com/drutopia/octavia/commit/c1b01be4b2b05a85ddea4147eefd86938d175ff2) Issue #56: fix spacing issue on views using columns within view modes (Nedjo Rogers)  
[170654a](https://gitlab.com/drutopia/octavia/commit/170654aa3477c70b50d52a3ad3d9621cd34573dc) Merge branch '55-views-columns' into '8.x-1.x' (Rosemary Mann)  
[1bd7acb](https://gitlab.com/drutopia/octavia/commit/1bd7acbab4b66fad8cb6cb1f8e8d19d2c7fcd9fd) Issue #55: fix views elements such as pager and RSS feeds displayed don't clear (Nedjo Rogers)  

## 1.0-alpha7

### drutopia_action
### drutopia_article
### drutopia_blog
### drutopia_campaign
[12261ea](https://gitlab.com/drutopia/drutopia_campaign/commit/12261eabda8cb344a3080630f81a37c91255f5d6) Update field_group to beta release (Nedjo Rogers)  
[91541a1](https://gitlab.com/drutopia/drutopia_campaign/commit/91541a16fecf5170cfc0493d319d0af9c67b8b3a) Merge branch '7-tweak-view' into '8.x-1.x' (Nedjo Rogers)  
[69611fe](https://gitlab.com/drutopia/drutopia_campaign/commit/69611fe732799bbc6c9248b5293c0be9f39b40f7) Issue #7: Tweak view to unset row class (Rosemary Mann)  
[e9ca1e1](https://gitlab.com/drutopia/drutopia_campaign/commit/e9ca1e1544829760c39b825a3de0ad404eb3b808) Merge branch '7-refactor-view' into '8.x-1.x' (Nedjo Rogers)  
[f3ebc28](https://gitlab.com/drutopia/drutopia_campaign/commit/f3ebc280c5a2b4572f3ba1d5669af245e4698d4e) Issue #7: refactor to use view based on search index (Rosemary Mann)  
[f68b724](https://gitlab.com/drutopia/drutopia_campaign/commit/f68b7249f1374fa71e2395d4568486aceba53009) Merge branch 'campaign-simple-card' into '8.x-1.x' (Nedjo Rogers)  
[3386bda](https://gitlab.com/drutopia/drutopia_campaign/commit/3386bdaa9d3bc3e06f097436db54741c4799e8f5) Add and configure simple card for campaign (Rosemary Mann)  
[8ddd14a](https://gitlab.com/drutopia/drutopia_campaign/commit/8ddd14ae3cb0af5bd05c9c6f97db3df77873ab40) Merge branch 'cleanup-campaign' into '8.x-1.x' (Nedjo Rogers)  
[3edfd3d](https://gitlab.com/drutopia/drutopia_campaign/commit/3edfd3d8476425bd2d84fd58ebb4c7ff38b9c5f4) Clean up campaign feature (Rosemary Mann)  
### drutopia_comment
### drutopia_core
[1fbd3a4](https://gitlab.com/drutopia/drutopia_core/commit/1fbd3a46a9a85051a23838110ffa05d43a60c740) Merge branch '29-view-modes' into '8.x-1.x' (Nedjo Rogers)  
[f6ba05b](https://gitlab.com/drutopia/drutopia_core/commit/f6ba05b2525fcc3ee5ae8e23f7f5a9d402162860) Issue #29: Add new view modes (Rosemary Mann)  
### drutopia_custom
### drutopia_event
### drutopia_fundraising
### drutopia
[58418bf](https://gitlab.com/drutopia/drutopia/commit/58418bfdc569de8aa309ecb61a084a466d883ec2) Everything is a checklist item.  Everything. (benjamin melançon)  
[bcefa42](https://gitlab.com/drutopia/drutopia/commit/bcefa427bd00502b13fabb30034b0022891424fc) Add instructions; link to docs; add headings; simpler language (benjamin melançon)  
[3f678d1](https://gitlab.com/drutopia/drutopia/commit/3f678d12a75a402375c6ddfc949b217ecfd5c9ed) Add issue template with checklist for new release (Rosemary Mann)  
[3d592a3](https://gitlab.com/drutopia/drutopia/commit/3d592a35d24ca36ea028e981a42f0584583d99be) Merge branch 'pin-config-sync' into '8.x-1.x' (Nedjo Rogers)  
[7058286](https://gitlab.com/drutopia/drutopia/commit/705828674faa9ae341a3050a49ce8057fdcb1653) Work around config sync growing pains (benjamin melançon)  
### drutopia_group
### drutopia_landing_page
[ed19dcf](https://gitlab.com/drutopia/drutopia_landing_page/commit/ed19dcf8403ae2a3a39adf7c6f79c5287503f3d6) Merge branch '8-hide-promotion' into '8.x-1.x' (mlncn)  
[d5b14c3](https://gitlab.com/drutopia/drutopia_landing_page/commit/d5b14c36e8335c518baecbb85a0f1965c080d532) Hide promotion options (Clayton Dewey)  
### drutopia_page
### drutopia_people
### drutopia_related_content
### drutopia_resource
### drutopia_search
### drutopia_seo
### drutopia_site
[d7cbcb4](https://gitlab.com/drutopia/drutopia_site/commit/d7cbcb4f685f434e0b2b656b7cbd1e9b3a2279f3) Merge branch '10-block-configuration' into '8.x-1.x' (Nedjo Rogers)  
[0490654](https://gitlab.com/drutopia/drutopia_site/commit/04906546efecd935ce6b095e9ecec7aaaaa7969d) Issue #10: Block configuration for slide (Rosemary Mann)  
### drutopia_social
### drutopia_storyline
[02bb10d](https://gitlab.com/drutopia/drutopia_storyline/commit/02bb10d4fad30a9b4ae3193dc3a57dc03f73a3c2) Update field_group to beta release (Nedjo Rogers)  
### drutopia_user
### octavia
[af4a624](https://gitlab.com/drutopia/octavia/commit/af4a6240b29f2440d1376876e5616896c4f30764) Merge branch '50-homepage' into '8.x-1.x' (Nedjo Rogers)  
[8c280f3](https://gitlab.com/drutopia/octavia/commit/8c280f377f5b9ad505ef80b8534b9fec256ce15c) Issue #50: fix spacing issue (Nedjo Rogers)  
[620df5b](https://gitlab.com/drutopia/octavia/commit/620df5b0e67e4674e0caebff21736a8e9fcebdeb) Issue #50: Homepage changes (Rosemary Mann)  
