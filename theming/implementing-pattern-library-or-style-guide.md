# Implementing a pattern library or styleguide

Drutopia's default base theme, Octavia, is built on the Bulma CSS framework.  Bulma, in turn, employs [Sass](https://sass-lang.com/) to process CSS and extend the ways one can work with CSS.  Sass gives us [powerful ways to customize Bulma](https://bulma.io/documentation/customize/concepts/) and, in turn, gives sub-themes of Octavia powerful ways to customize it.  (We recommend Sass for themes not extending Octavia or using Bulma, also!)

Using a styleguide can be seen as an echo or continuation of the Drupal 8 philosophy of pushing responsibility to dependencies (in Drupal 8's case, these dependencies are managed by composer).  In a styleguide, the dependencies can be managed by Sass.

This guide covers how to apply a styleguide to your site.  (The guide for how to structure Sass for a pattern library with styleguide that can be used by multiple projects still needs to be written.)

When implementing a theme based on a pattern library or style guide, adapt the code for your templates from the style guide's own templates as much as possible.  That is, avoid simply using your browser's inspector to determine the HTML structure and classes your template needs.  Even 'View source' would be better, because the DOM structure in your inspector may have been manipulated by JavaScript.

For example, in [Agaric's style guide](https://gitlab.com/agaric/patternlibrary) by [Todd Linkner](https://tlda.co), inspecting the static layout for the design (using the exact same styles, fonts, and JavaScript that the live site will) shows a first line of the navigation bar with HTML code like this:

```
  <nav class="navbar transparent-header" id="navbar">
```

Viewing the [Handlebars template used to generate the header section of the static layout](https://gitlab.com/agaric/patternlibrary/blob/master/static-layouts/design-source/templates/header.hbs) (or viewing source) shows that the same line of HTML should in fact start out as, and be in our template as, this:

```
  <nav class="navbar" id="navbar">
```

The extra 'transparent-header' (which never shows up in any code but the SCSS/CSS as a searchable string like that) is because a fancy bit of [custom JavaScript included in the styleguide](https://gitlab.com/agaric/patternlibrary/blob/master/agaric/js/script.js) is constructing the class, which is used to determine the header background, depending on the viewers scroll position on the page.

## Removing Unwanted HTML Wrapping Elements

You may want to remove a lot of unnecessary `div`s when creating your own subtheme (almost all themes are ultimately a subtheme of [Drupal core's stable theme](https://www.drupal.org/docs/8/core/themes/stable-theme)), and you're especially likely to want to do this when implementing a style guide on top of a subtheme— you'll want the markup your site produces to match the markup called for by the style guide.

A quick inspect or view source (with [Twig debugging turned on](https://www.drupal.org/docs/8/theming/twig/debugging-twig-templates)) reveals a highly superfluous div, preceded by the template to blame it on:

```
<!-- BEGIN OUTPUT from 'themes/contrib/bulma/templates/block/block--system-branding-block.html.twig' -->
<div id="block-agarica-branding" class="block block-system block-system-branding-block clearfix">
  // yada yada yes there's a closing div too.  Remember to remove them in pairs.
</div>
```

So we copy [bulma/templates/block/block--system-branding-block.html.twig](https://cgit.drupalcode.org/bulma/tree/templates/block/block--system-branding-block.html.twig) to our own theme's template location and prepare to remove the divs.  And they... aren't there.  With comments and extraneous content removed, it looks like this:

```
{% extends "block.html.twig" %}

{% block content %}
  // Stuff in here is whatever, take it or leave it, but not the divs we're after
{% endblock content %}
```

Well it's extending its own basic block template so we go look at [bulma/templates/block/block.html.twig](https://cgit.drupalcode.org/bulma/tree/templates/block/block.html.twig) and...  still no divs, but we see it `{% extends "@stable/block/block.html.twig" %}` so now we have to find that.  The `@stable` part of that is a [barely documented](https://www.drupal.org/docs/8/theming-drupal-8/including-part-template) feature in which a namespace can be used for a module or theme rather than a full path (which can change from site to site or when a site project is reorganized).  The [change record introducing the stable theme](https://www.drupal.org/node/2580687) suggests that it's not necessary to specify which theme is being extended, but that [doesn't seem to be the case](https://www.drupal.org/project/drupal/issues/2387069) in a robust sense.

> NOTE: The `extends` command extends exactly what it says it extends.  Template suggestions are no longer a part of it.  Therefore the `core/themes/stable/templates/block--system-branding-block.html.twig` has no bearing whatsoever on our `block--system-branding-block.html.twig`.

And finally, there it is, in [Drupal core/themes/stable/templates/block/block.html.twig]():

```
<div{{ attributes }}>
  {{ title_prefix }}
  {% if label %}
    <h2{{ title_attributes }}>{{ label }}</h2>
  {% endif %}
  {{ title_suffix }}
  {% block content %}
    {{ content }}
  {% endblock %}
</div>
```

That div tried to hide behind layers of templates that extended it, but we tracked it down.

Now we can copy its contents back into our `block--system-branding-block.html.twig` (note that we don't want to override `block.html.twig` and lose that div *everywhere*, at least not just yet) and—here's the crucial part—remove the `extends` line.  Now replace `{{ content }}` inside the `{% block content %}` and `{% endblock %}` tags with the ... contents ... of your own matching tags.  Now you can delete the empty leftover extra `{% block content %} {% endblock content %}` tags. 

And finally, finally, we can delete those divs, and if we're sure we don't want the attributes (like classes) that were put on those divs we don't need to move those anywhere, and if we know we don't want a block title, we can remove that too, so that the whole thing looks like this:


```
{% block content %}
  // Stuff in here is whatever, take it or leave it, but not the divs we're after
{% endblock %}
```

## Adding a class or other attribute to a parent theme's template

Octavia takes a conservative approach to replacing Bulma's and Stable's templates, preferring to add a class rather than override all the HTML, so there's lots of examples of this.

First, make certain the `<div>` or other HTML element you want to add classes to is in fact using an `attributes` variable and not hard-coding them.  In this example, we want to add a `navbar-menu` class to the, well, navigation bar menu to match the [static design header template](https://gitlab.com/agaric/patternlibrary/blob/master/static-layouts/design-source/templates/header.hbs).  Rather than change the `<nav>` to the `<div>` that the design template uses, we'll keep the more semantic `<nav>` that Drupal's Stable theme is providing to us (by way of Bulma and Octavia).  The navigation menu appears in a block that starts like this in the source:

```
<!-- BEGIN OUTPUT from 'themes/contrib/bulma/templates/block/block--system-menu-block.html.twig' -->
<nav role="navigation" aria-labelledby="block-agarica-main-menu-menu" id="block-agarica-main-menu" class="menu">
```

So we do our thing of looking at [Bulma's block--system-menu-block.html.twig](https://cgit.drupalcode.org/bulma/tree/templates/block/block--system-menu-block.html.twig) and we see that it, in its own words, `extends "@stable/block/block--system-menu-block.html.twig"`.  It's our lucky day because it also gives an example of adding classes to the attributes which *will be used by the template it is extending*.

> NOTE: Well actually that's what we were walking our way through the templates to confirm; if you open [Drupal core's Stable theme's block--system-menu-block.html.twig template](https://cgit.drupalcode.org/drupal/tree/core/themes/stable/templates/block/block--system-menu-block.html.twig) you'll see that rather than a hard-coded `class="menu"` or something like that it has an `{{ attributes }}` variable inserted into the `<nav>` element.
>
> > NOTE: Well actually it's a bit more complicated than that in this case; the opening tag of the navigation element looks like this:
> > `<nav role="navigation" aria-labelledby="{{ heading_id }}"{{ attributes|without('role', 'aria-labelledby') }}>`
> > All that is doing, though, is excluding two attributes (`role` and `aria-labbelledby`) from being output with the variable because they are already included in the HTML.  Classes will still be output in the place where the `attributes` variable is inserted.

We can add a class without having to duplicate any HTML portion of a template by following Bulma's example:

```
{% set classes = classes ?? ['menu'] %}
{% set attributes = attributes.addClass(classes) %}
```

> NOTE: Bulma here is using `??`, [Twig's null-coalescing operator](https://twig.symfony.com/doc/2.x/templates.html#other-operators), which will only set `classes` to 'menu' *if there aren't any classes already*. 
> > NOTE: Bulma is probably being a bit too polite or cautious here.  There's no `classes` variable defined in the template its extending so there's no way for `classes` to have been defined.  While it's essential to never try using [Twig's merge filter](https://twig.symfony.com/doc/2.x/filters/merge.html) on an undefined variable because it would take down the whole site— with an error in the logs 'PHP message: Uncaught PHP Exception Twig_Error_Runtime: "The merge filter only works with arrays or "Traversable", got "NULL" as first argument."'  Because this error can take down a site, it makes sense to never trust that a variable exists before merging new things into it, and using the null-coalescing operator to ensure that the `classes` variable were at least set to an empty array like this `{% set classes = classes ?? []|merge(['menu']) %}` might make sense.  But if Bulma wants that class, they should just initiate classes to it.  In most cases, though, one uses the `addClass()` method on the `attributes` object, as you'll see us do below. 

We make our own template targeted to just the main menu.  The file name to use for our template in order to do that is provided helpfully in a "FILE NAME SUGGESTIONS" comment in our page's HTML source (courtesy of enabling the Twig debugging).  That name, and so the name of our template file, is `block--system-menu-block--main.html.twig`.  If you looked at [Bulma's block--system-menu-block.html.twig](https://cgit.drupalcode.org/bulma/tree/templates/block/block--system-menu-block.html.twig) you'll have noticed it added two more classes conditionally if the region equaled 'footer'; in our template file we don't need an `if` statement like Bulma is using because the block template itself is already conditionally loaded only for the main menu based on its name (the extra `--main`).  Therefore we can go straight to setting the `navbar-menu` class:

```
{% set attributes = attributes.addClass('navbar-menu') %}
```

And if we save this and reload caches (`drush cr`), our menu will... entirely disappear.

The reason for this is that we forgot to extend a template!  Which template should we extend?  It could make sense to extend Stable's system menu block template directly (the template that Bulma's system menu block template is extending), given that most of what Bulma is adding is related to the footer region only, and we don't plan on putting our main menu in the footer.  But the most conservative, light-touch approach is to extend the template we're replacing.  It does add a couple classes even when the footer isn't in play, and it might do more in the future.  Putting `{% extends "@bulma/block/block--system-menu-block.html.twig" %}` will do the trick.

> NOTE: Leaving off the `@bulma/` part in case Octavia, the theme we're currently using, implements the system menu block temlpate is a great idea but currently [won't work](https://www.drupal.org/project/drupal/issues/2387069).

Now our theme's (or pattern library's) [block--system-menu-block--main.html.twig](https://gitlab.com/agaric/patternlibrary/blob/master/agaric/templates/block/block--system-menu-block--main.html.twig) template is just this:

```
{% extends "@bulma/block/block--system-menu-block.html.twig" %}

{% set attributes = attributes.addClass('navbar-menu') %}
```

> IMPORTANT: The **extends** statement always needs to be at the top of the template file.
