
Do you want to help out without having to pull code?  (Or push code, or otherwise fold, spindle, or mutilate code?)

Here's how!

Search for issues that are both a contrib dependency (which includes Drupal core because, hey, everything is contributed from the perspective of ) and Help Wanted

Pick an issue.  Follow the link to the underlying drupal.org issue

If it has a patch read to be reviewed (its status should be **Needs review** if it does), copy the URL to the patch.

Go to https://simplytest.me and select the latest version of Drupal core and


... or maybe follow https://dri.es/we-made-drupal-a-lot-easier-to-evaluate
