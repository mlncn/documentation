# Personas for Solidarity


**Audience Groups**
 
* Supporter
* Potential Supporter
* Organizer
* Donor
 
 
## Primary Audience Groups

### Supporter
 
**Personal Information**
Age: 30
Language: Linguistically diverse (first language is not English)
Reading Level: 5th Grade 
Technical background: Regularly uses Facebook, but gets frustrated easily with unintuitive technology
Access to technology: primarily on phone, limited data plan
Security needs: activity needs to be protected from state surveillance and right wing trolls
 
**Website Needs**
* Keep up with where a campaign is at
* Learn how best to support a campaign
* Contact an organizer to get more involved
* Commit to take action in support of a campaign
* Learn the background information about a campaign
* Tell others about actions they can take
* Prepare for an upcoming event
* Plan an action with others
* Donate to the group
* Donate to the campaign
* Find out when the next event is
* Deepen ties with fellow organizers and supporters
* Promote and encourage the work of others taking action
 
**Websites that Supply Similar Information **
* Group’s Facebook page
* Ally group websites
 
**Level of Familiarity with Subject Matter**
* Medium - cares about the issues, but might need more background information for a deeper understanding
 
 
### Potential Supporter

**Personal Information**
Age: 25
Language: Linguistically diverse (first language is not English)
Reading Level: 5th Grade 
Technical background: Regularly uses Facebook, but gets frustrated easily with unintuitive technology
Access to technology: primarily on phone, limited data plan
Security needs: activity needs to be protected from state surveillance and right wing trolls
 
**Website Needs** 
* Learn what the vision and politics of the group is
* Learn what impact the group has had
* See what other groups and people support and work with the group
* Learn what campaigns are currently happening
* Decide whether to take action for a campaign
* Contact the group to learn more
* Learn more about the issues
* Tell others about the group
* Tell others about a campaign
* Find out when the next event is
* Donate to the group
* Donate to a campaign
 
**Websites that Supply Similar Information**
* Group's Facebook Page
* Ally group websites
 
**Level of Familiarity with Subject Matter**
* Low - Is interested in their work, but needs to learn more about the actors involved and the group’s strategy for change
 
### Organizer

**Personal Information**
Age: 38
Reading Level: College graduate
Technical background: Regularly uses Facebook, including as an admin, regularly uses Twitter, can use a variety of tools but appreciates the ones that value design and ease of use.
Access to technology: primarily on phone, but also uses a computer - average internet plan
Security needs: activity needs to be protected from state surveillance and right wing trolls
 
**Website Needs**
* Post updates on campaigns
* Post actions
* Share actions via newsletter
* Share actions via social media (Facebook, Twitter, Mastodon)
* Share actions via text message
* Set goals for actions
* Track supporter activity (who is helping out a lot, who is less reliable, etc.)
* Thank supporters
* Reach out directly to supporters
* Thank donors
* Create and share resources
* Post events
* Recruit for events
* Post updates to the site
* Post updates via social media
* Share campaigns
 
 
Websites that Supply Similar Information 
Facebook page
Ally group websites
 
Level of Familiarity with Subject Matter
High
 

### Donor
 
**Personal Information**
Age: 43
Reading Level: college
Technical background: Low, uses technology like Facebook, but is not a digital native
Access to technology: medium to high, uses phone and computer frequently
Security needs: activity needs to be protected from state surveillance and right wing trolls
 
**Website Needs**
* Learn what the vision and politics of the group is
* Learn what impact the group has had
* See what other groups and people support and work with the group
* Learn what campaigns are currently happening
* Know what financial needs the group has
* Know how well the group manages their money
* Contact the group to learn more
* Donate to the group
* Donate to a campaign
 
**Websites that Supply Similar Information**
* Facebook page
* Ally group website
 
**Level of Familiarity with Subject Matter**
* Varies from low to high - needs concise, high level info on what the group is about and the impact they have
