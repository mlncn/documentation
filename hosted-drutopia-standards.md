# Hosted (SaaS) Drutopia Standards

To be recommended by Drutopia as a host, these qualifications—at
minimum—must be met:

*  All people or organizations hosted on the platform are members of the Drutopia cooperative, and the membership fee is passed on to the cooperative.
* The resources dedicated to each site meet the Drupal 8 system requirements.
* The host must support SSL (https://)
* Drupal core must be supported with security updates
* All approved Drutopia contributed modules must be supported with security updates (supporting additional is allowed of course)
* All approved Drutopia configuration sets must be available for use (supporting additional is allowed of course)
* All approved Drutopia install profiles must be available on new site launch (supporting additional is allowed of course)
