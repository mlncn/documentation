Reporting bugs
==============

1. Make sure the bug isn’t already resolved. Search for similar issues in the bug category
1. Make sure you can reproduce your problem on a clean installation of Drutopia.
  * If you’re also testing on your own Drutopia development instance, be sure to use the Master release branch, aka the Tests Passed channel.
  * If possible, submit a Pull Request with a failing test, or;
  * if you'd rather take matters into your own hands, try fix the bug yourself.
1. Make a report of everything you know about the bug so far by opening an issue in GitLab about it.

When the bug is fixed, you can usually expect to see an update posted on the reporting topic.

