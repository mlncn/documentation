docs.drutopia.org
=================

The site you're reading right now is hosted by [Read The Docs](https://readthedocs.org/) using [Sphinx](http://sphinx-doc.org) and r

Just copy and paste those bits when needed

```
sudo apt install python-pip
sudo pip install sphinx sphinx-autobuild
sudo pip install recommonmark
```

If you're a fan of ReStructuredText, you're in good company— it's Read The Docs' recommended format.  So long as you're willing to help others with the syntax from time to time, don't let us hold you back from flexing the power of rst.
