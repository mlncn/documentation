# ***EARLY DRAFT*** Software Cooperative Member Guide

To become a member of the Drutopia Software Cooperative:

* Pay annual dues ($10 a year minimum, $50 a year suggested) [TODO or work out verified non-monetary contributions]
* Agree to Drutopia's <a href="/goals.html#points-of-unity">Points of Unity</a>.
* Abide by Drutopia's <a href="/drutopia-code-of-conduct.html">Code of Conduct</a>.

The Code of Conduct is one way to help  us recognize and strengthen the relationships between our actions and their effects on our community.

Communities mirror the societies in which they exist and positive action is essential to counteract the many forms of inequality and abuses of power that exist in society.

If you see someone who is making an extra effort to ensure our community is welcoming, friendly, and encourages all participants to contribute to the fullest extent, we want to know.  [TODO How?]
