Drutopia Base Distribution
==========================

`Drutopia Base <https://gitlab.com/drutopia/drutopia/>`_ is the flagship libre software product of the Drutopia initiative. It provides the building blocks for the features grassroots groups need to organize, inspire, and mobilize.

Features
--------

Drutopia Base comes with:

* `Actions <https://gitlab.com/drutopia/drutopia_action/>`_
* `Articles <https://gitlab.com/drutopia/drutopia_article/>`_
* `Blog Posts <https://gitlab.com/drutopia/drutopia_blog/>`_
* `Campaigns <https://gitlab.com/drutopia/drutopia_campaign/>`_
* `Groups <https://gitlab.com/drutopia/drutopia_group/>`_
* `Profile Pages <https://gitlab.com/drutopia/drutopia_people/>`_
* `Octavia <https://gitlab.com/drutopia/octavia/>`_ : A `Bulma-based <https://bulma.io/>`_ theme

Roadmap
-------

See `Drutopia milestones on GitLab <https://gitlab.com/groups/drutopia/-/milestones/>`_
