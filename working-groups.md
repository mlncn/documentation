# Working Groups

## User experience working group  #UX
Team lead: Clayton Dewey clayton@drutopia.org   
The user experience working group is focusing at the moment on laying the foundation for a fantastic user experience for both contributors and supporters of Drutopia as well as the end user of the Drutopia tools we are building.

To do this we are doing the following -  
**Contributor UX**
1. Expanding on the current [README.MD](https://gitlab.com/drutopia/drutopia-distribution/issues/20)
2. Writing a [CONTRIBUTING.MD](https://gitlab.com/drutopia/drutopia-distribution/issues/19)
3. Defining a [Code of Conduct](https://gitlab.com/drutopia/drutopia-distribution/issues/17)

**End User UX**
1. Conducting user research through [surveys](https://gitlab.com/drutopia/drutopia-distribution/wikis/survey) and [interviews](https://gitlab.com/drutopia/drutopia-distribution/wikis/interview)
2. Adding the survey to [drutopia.org](http://drutopia.org/)

I am looking for both help in the following ways
1. Joining the working group in a regular or semi-regular way
2. Completing the survey (for now answer the questions and email them to me)
3. Being interviewed (email me letting me know and we can set up a time!)
4. Sending me contact info for folks you think would be down to fill out the survey or be interviewed
5. Share the survey directly with folks (again have them email me their answers for now)
6. Interview people directly (email me and we can walkthrough what that looks like, which shouldn't take long to do)
7. Adding the survey as a form on Drutopia (if you can do this, let me know and I'll add you as a contributor to the project)


## Project management working group
Team lead: Leslie Glynn leslie@drutopia.org  
Goal: to help the Drutopia project through
- tracking the objectives/issues of each of the working groups
- documenting and tracking the project schedule and milestones
- assisting the leadership team through reporting on project status 

## Users working group
Team lead: Leslie Glynn leslie@drutopia.org  
Goal: to help the Drutopia project through
- communicating with potential users of Drutopia to identify features of interest
- finding commonality of desired features among the various users
- assisting the leadership team in defining features and priorities in the Drutopia roadmap

## Documentation working group
Team lead: Rosemary Mann rosemary@drutopia.org  
We’ll be tackling all areas of documentation including end user documentation for non-technical users. While some of these tasks will not happen until we’re further along in the development cycle, we want to ensure that solid documentation goes hand in hand with development.

[Documentation issues](https://gitlab.com/groups/drutopia/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name[]=Documentation)

## Outreach/marketing working group
Team lead: Ben Melançon ben@drutopia.org  
Outreach/marketing seeks to ensure that every web designer, styler, developer, and site builder knows of Drutopia's goals and approaches, and knows how to participate in Drutopia if they are interested.
 We also work to communicate the potential of Drutopia to organizations who need websites, and to distill the membership offering so they can make the decision to join.

## Development working group
Team lead: Nedjo Rogers nedjo@drutopia.org

The development working provides technical guidance and support to the Drutopia initiative.

[Development issues](https://gitlab.com/groups/drutopia/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=Development)

### Technical specifications
* Produce and maintain technical specifications of Drutopia-compatible features and distributions.
* Evaluate and make recommendations on specific solutions to be used in Drutopia. Example: will there be a standard base theme?

### Code development
* Contribute to and maintain Drutopia extensions. See the [list of extensions used in Drutopia](http://www.drutopia.org/drutopia-drupal-extensions).

### Current priorities
Looking to get involved? Here are some current priorities and gaps.
* Review of the draft technical guide.
* Open issues in the [Configuration Sync](https://www.drupal.org/project/issues/config_sync) module.
* Lead development for planned [Configuration Merge](https://www.drupal.org/project/config_merge) and [Block Theme Sync](https://www.drupal.org/project/block_theme_sync) modules.

## Co-op working group
Interim team lead: Rosemary Mann rosemary@drutopia.org  
One of the core aims of Drutopia is to create a different kind of economic model. To this end we’re are envisioning the creation of a platform co-op where end user groups are members. We have also explored the idea of a multi-stakeholder co-op so that worker members are also included. This working group will tackle the research, business planning, and organizational work to create a new co-op.

## Features working group
Interim team lead: Nedjo Rogers nedjo@drutopia.org  
At its most basic, a distribution is a set of features that will work seamlessly together but that can also be used separately or, ideally, in other distributions. Building out those features is much like site building but with the extra complexity of each being a generic solution and with challenges of interoperability and consistency. The Features working group will take on the concrete work of developing and fine-tuning the features that will become Drutopia.