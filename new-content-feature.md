# How to produce a new content feature 

These instructions will use the example of a yet to be built “resource”  feature.


## Naming conventions: 

* See also the technical guide section on [naming and namespaces](http://docs.drutopia.org/en/latest/technical-guide.html#naming-and-namespaces).
* The content type will be called: `resource` (singular)
* Any field that is specific to this feature will be prefixed with `resource_`, so for example a reference field to a vocabulary specific to resource might be called `field_resource_type`.
* More naming conventions will be discussed in the relevant sections such as Pathauto, Views and Creating your feature.


## Fields: what fields are re-used, what are required

* Many of the core field are reused between features.
* These include
 * `field_summary` (required)
 * `field_body_paragraph`
 * `field_image`
 * `field_topics`
 * `field_tags`
* These fields should be added and configured as needed. Because we are using paragraphs for our main body, a required summary field is necessary for many display purposes and should always be included.
* When adding a new field, first determine if it will be specific to your feature or is perhaps a more generic field. If you think it is more generic, talk to a tech lead to consult. If it is specific, give it a name that includes the content type name, e.g. `field_resource_type`.
* When adding reused fields, if you have queries about any configuration, look at another content type feature for guidance.


### Paragraph types:

* Drutopia features use paragraphs as the main body field (`field_body_paragraph`). For each feature you can set what paragraph types should be included. All `field_body_paragraph` fields should have at least text, image and file paragraphs. Other paragraph types can be included if they seem useful to your content type.
* If you think you need a new paragraph type, discuss with a tech lead.


### Metatag

* The metatag field (`field_meta_tags`) should be included in every content type feature.

### Form display

* The traditional Body field (`field_body`) has been maintained for migration purposes. It should always be hidden on the form display.
* Refer to another content feature such as `drutopia_article` as a model for the general ordering and setting guidelines.
* For the Body paragraph field, ensure the following settings are set:
 * Edit mode: Open
 * Add mode: Buttons
 * Default paragraph type: Text


### Display

* We do not use the default view mode, so ensure there are no fields showing.
* Enable the view modes that you envision using. At the very least there needs to be a full view mode and a teaser.
* We are using Display Suite so you need to set a Display Suite view mode before doing the configuration. The full content display is often using the “Three column stacked – 25/50/25” layout.
* Look to other content types to see how they are using some of the other more specialized view modes and which Display Suite layout they are using.
* If you want a Promoted block for the home page, you will need to format a Card view mode, see below for further details.
* For some of the view modes, you will need to add  one or more field groups so that the theming will work correctly. Drutopia Article has the most developed view modes so it is a good example to work from.
* Where labels should be hidden, please use “Visually hidden” wherever possible.
* For the Image field, use Responsive image and set to wide or narrow as seems appropriate for your view mode.
* If you feel that you require a new view mode for your content type, please consult a tech lead.
* Enable and configure the search_index view mode with all labels hidden.


#### Formatting guidelines for card view mode

* Enable `card` in Custom display settings.
* Ensure this view mode is using the Display Suite one column layout.
* Add the image field to the content area, selecting responsive image, narrow and linked to content.
* Next, create a field group for the card view mode.
* This is a group of type HTML element and must be named Card content, `card_content`. Settings can be left at default.
* Add this field group to the content area.
* Then place into it the desired fields:
 * Title, linked to content.
 * Summary, trimmed to 180 characters.
 * Add a `type` field or `tags` as desired.
* Save your card settings.

Note: When using the card view mode in a view, ensure that the field “Add views row classes” under setting is unchecked.


#### Display Suite fields

Where there is a need for specialized display in a view mode, we rely on Display Suite fields. Examples:

* To concatenate two fields, add a Display Suite token field and use tokens to concatenate the fields.
* If there is a need to place a block in the view mode:
    * Create the block as e.g. a view.
    * Add a Display Suite block field, selecting the block in question.

Whenever creating a Display Suite field, configure to limit it to only the particular entity bundles that are applicable. For example, if the field is only for events, limit it to the `event` bundle.


## Pathauto

* A content type should always have an associated Pathauto pattern.
* Create a new pattern:
 * Type: Content
 * Path: `resources/[node:title]`
 * Select the content type it applies to (for example Resource)
 * Label:  Node resource


## Views

* As of the alpha5 release, content type views are based on a search index for that content type. Eventually we'll be adding facets to these listing pages.


### Create an index for your content type

* Ensure you have `drutopia_dev` and `entity_clone` installed and enabled.
* Clone the node_dev index to create a search index for each content type limiting it to that one content type. Give it the same name as the content type and update the description.
* Configure the search_index view mode for your content type and use it in the rendered HTML field on the search index.
* Add any fields that are specific to the content type, for example a type vocabulary. This will be needed for any field that we want to use for facets.
* Flush caches before proceeding.


### Create a view

* Create a new view based on the search index for your content type using the singular name of the content, for example `resource`.
* Most content types will need a page display and ideally a block display for promoted content. Other displays may be needed for a particular content type use case.


### Page display

* The page display should use the plural in the path, for example `/resources`.
* A menu item should also be set here for easy export.
 * Give it a normal menu entry, with the parent menu <main navigation>
 * Give it a title, for example “Resources”, and a description, such as “Get connected with our resources.”
* Under format settings, ensure that Add views row classes is unchecked.
* Under format, show, use the rendered entity selecting an appropriate view mode (often card).
* Sort criteria
 * Sticky
 * Content: Authored on (desc)
* Set the machine name to `page_listing`
* Set caching to none.


### Block display

* Create a block display for promoted content.
* Display name: Block Promoted
* Under Format choose Content and the Card view mode.
* Add a filter Content: Promoted to front page (= Yes)
* Limit to 4 items.
* Under Advanced, give it the machine name: `block_promoted`


## Creating feature and determining where fields go

* To create a Drutopia feature you need to enable the Features and Feature UI modules.
* Via the features interface (`admin/config/development/features`), start by ensuring that you have Drutopia selected as your features bundle.
* In Drupal 8 much of the features bundling happens automatically. By using the assignment plugins at their default, your configuration should be assigned to a new feature that will be shown as unexported.
* The included configuration link will show what is included in a general way.
* The link to the feature itself will take you to a page where you can review the configuration in more detail, adding or removing any items if necessary.
* The most important areas to review are the field storage and field instance. Field storage should only include fields that are unique to your content type. Field instance will include all field used.
* If you have added a new shared field (after consultation) that field storage will need to be added to Drutopia Core and this needs to be done ahead of creating the new feature.
* The machine name of the feature will be `resource`. (Question: This is because it's automatic or should it be edited to `drutopia_resource` by convention?)
* Edit the **Description** if needed.
* Ensure Drutopia is set as the **Bundle**.
* Set the **Version** to `8.x-1.0-alpha1` if it's the first time.  Once accepted into Drutopia, it should match the stability of the Drutopia distribution.
* To add the search_index view mode you will need to edit the feature, allow conflicts (as this view mode will be added to the seach feature by deafault) and manually add.
* Export your feature.  (If using **Write**, you can set the **Path** to `modules/contrib` presuming you intend to contribute it.)
* After export, manually edit the info file to give it the name ‘Drutopia Resource’.


## Adding .json file

* Drutopia uses Composer so each feature needs a `.json` file.
* The `.json` file needs to includes direct dependencies only.
* Use another feature as a model and be sure to include any new modules you have needed for your feature.


## Contribute the feature module

* Initialize a git repository in the feature module.  For example, `cd modules/contrib/drutopia_resource`, `git init`, `git checkout -b 8.x-1.x`, `git branch -d master`, `git add .`, `git commit -m "Create the Drutopia Resource feature"`.
* Go to the [Drutopia organization on GitLab](https://gitlab.com/drutopia/) and create a new project with the same folder/machine name as your feature module (or, if you do not have permissions in the Drutopia organization, start the project in your own namespace).  Make it a public project (**Visibility Level** Public).
* Follow GitLab's instructions for an existing folder which we haven't already done; for example `git remote add origin git@gitlab.com:drutopia/drutopia_resource.git` and `git push -u origin 8.x-1.x`.


## Adding permissions via config_actions

* Permissions are not directly exportable so they need to exported via config_actions.
* See the page on [adding user permissions](add-user-permissions.html).


## Default content

* Ideally a content type includes default content.
* See detailed instructions: [Producing and exporting default content for Drutopia](development/producing-and-exporting-default-content.html).


## Settings provided by other modules

As a general guideline, we can export to regular features _only custom configuration that we have ourselves created_, such as a view mode or a field. If we're configuring using a settings form, that configuration will already be provided elsewhere (for example, by the `chosen` module). A hint that this may be occurring is that the name of the configuration includes another module's name, such as `chosen.settings.yml`. Another hint is if the configuration does not show up by default as available for adding to your feature (that is, you have to check the option to allow conflicts before it will show up).

We have two options for such configuration:

* The first is to add it to the install profile, which is allowed to override configuration provided by other modules. This option is appropriate if:
    * The configuration should always be active on all sites; and
    * The configuration is not required by other configuration or functionality. This consideration is because the install profile is typically installed last and nothing should require the install profile.
* Otherwise, we can add it as a config action per the `config_actions` module. See the [Config Actions documentation](http://config-actions.readthedocs.io/en/latest/plugins.html).


For example, to change a configuration option provided by the `realname` module, you might add a file `config/actions/realname.settings.yml` with the following content:

```yml
path:
  - pattern
plugin: change
actions:
  'full name':
    value: '[user:field_user_first_name] [user:field_user_last_name]'
```

(where `full name` is an arbitrary key identifying the change).

