# Quickstart

There are two quick ways to get Drutopia up and running: PHP on your local computer or a full, production-like environment provided by a virtual machine.

In both cases we'll end with the Drutopia distribution codebase, built by Composer, in a directory called `my-drutopia-site` (please change this to whatever you would like in the instructions below!).

## PHP

### Prerequisites

1. PHP (5.5.9+, 7.1+ preferred; test in a terminal with `php --version`).
2. [Composer](https://getcomposer.org/download/).

> **Note**: If you have composer installed [globally](https://getcomposer.org/doc/00-intro.md#globally) you can use `composer` instead of `./composer.phar` in the first command.

### Instructions

Run these commands in a terminal

1. `./composer.phar create-project drutopia/drutopia_template:dev-master --keep-vcs --no-interaction my-drutopia-site`
2. `cd my-drutopia-site/web && php -S localhost:8008`
3. Open [localhost:8008](http://localhost:8008/) and follow the installation instructions; leaving everything at defaults will be fine.

See [the README](https://gitlab.com/drutopia/drutopia_template#readme) that was downloaded in that step for more.

## Virtual machine

### Prerequisites

1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) (check your app store or package manager)
2. Install [Vagrant](https://www.vagrantup.com/downloads.html) (check your app store or package manager)

### Instructions

1. `git clone git@gitlab.com:drutopia/drutopia_vm.git my-drutopia-site`
2. `cd drutopia_vm`
3. `vagrant up` (this runs a site install via drush but [unfortunately no Drutopia features are enabled](https://gitlab.com/drutopia/drutopia/issues/151)).
4. Log in at [my-drutopia-site.test/user](http://my-drutopia-site.test/user) with the credentials username: admin, password: admin
5. Enable Drutopia modules at [my-drutopia-site.test/admin/modules](http://my-drutopia-site.test/admin/modules)

You can access the virtual machine (for use of Drush etc) with `vagrant ssh`.
