# Extension selection criteria and candidate extensions

See also Drutopia [technical guide](technical-guide) and module usage information in #42.

## Selection criteria
* Has a stable release on drupal.org, ideally with Drupal security team coverage.

## Supported extensions
These are extensions already supported.

### [Configuration Update Manager](https://www.drupal.org/project/config_update)
* Supplements the core Configuration Manager module, by providing a report that allows you to see the differences between the configuration items provided by the current versions of your installed modules, themes, and install profile, and the configuration on your site.
* Required by Features.
* Developer-only: should not be enabled by default.

### [Features](https://www.drupal.org/project/features)
*  Enables the capture and management of features in Drupal. A feature is a collection of Drupal entities which taken together satisfy a certain use-case.
* Developer-only: should not be enabled by default.

## Candidate extensions

### Modules

#### [Address](https://www.drupal.org/project/address)
* Provides functionality for storing, validating and displaying international postal addresses.

#### [Admin Toolbar](https://www.drupal.org/project/admin_toolbar)
* Improves the default Drupal Toolbar (the administration menu at the top of your site) to transform it into a drop-down menu, providing a fast access to all administration pages.

#### [Advanced Editor Link](https://www.drupal.org/project/editor_advanced_link)
 * Enhanced attributes for links.
 * Integrates with LinkIt.

#### [Block Visibility Groups](https://www.drupal.org/project/block_visibility_groups)
* Allows the site administrator to easily manage complex visibility settings that apply to any block placed in a visibility group.

#### [Chaos Tools](https://www.drupal.org/project/ctools)
* Provides functionality that didn't make it into Drupal Core 8.0.x.

#### [Contact Storage](https://www.drupal.org/project/contact_storage)
* Provides storage for Contact messages, which are fully-fledged entities in Drupal 8.
* In use on drutopia.org.

#### [Devel](https://www.drupal.org/project/devel)
* A suite of modules containing fun for module developers and themers.
* Developer-only: should not be enabled by default.

#### [Entity API](https://www.drupal.org/project/entity)
* Used for improvements to Drupal 8's Entity API which will be moved to Drupal core one day.
* Required by Profile.

#### [Entity Legal](https://www.drupal.org/project/entity_legal)
* Provides a solid, versionable, exportable and flexible method of storing legal documents such as Terms and Conditions and Privacy Policies.

#### [Entityqueue](https://www.drupal.org/project/entityqueue)
* Allows users to create queues of any entity type. Each queue is implemented as an Entityreference field, that can hold a single entity type.

#### [Entity Reference Revisions](https://www.drupal.org/project/entity_reference_revisions)
* Adds a Entity Reference field type with revision support.
* Required by Paragraphs.

#### [Field Group](https://www.drupal.org/project/field_group)
* Groups fields together. All fieldable entities will have the possibility to add groups to wrap their fields together. Fieldgroup comes with default HTML wrappers like vertical tabs, horizontal tabs, accordions, fieldsets or div wrappers.

#### [Honeypot](https://www.drupal.org/project/honeypot)
* Uses both the honeypot and timestamp methods of deterring spam bots from completing forms on your Drupal site.

#### [LinkIt](https://www.drupal.org/project/linkit)
 * Used for internal links (to nodes, taxonomy terms, etc.)
 * Use the 5.x branch, majorly refactored.

#### [Maxlength](https://www.drupal.org/project/maxlength)
* Allows you to set maximum length of any field on any form making use of the form API.

#### [Paragraphs](https://www.drupal.org/project/paragraphs)
* Comes with a new "paragraphs" field type that works like Entity Reference's. Simply add a new paragraphs field on any Content Type you want and choose which Paragraph Types should be available to end-users. They can then add as many Paragraph items as you allowed them to and reorder them at will.

#### [Pathauto](https://www.drupal.org/project/pathauto)
* Automatically generates URL/path aliases for various kinds of content (nodes, taxonomy terms, users) without requiring the user to manually specify the path alias.

#### [Profile](https://www.drupal.org/project/profile)
* Provides configurable user profiles.

#### [Social Media Share](https://www.drupal.org/project/social_media)
* Allows the user to share current page to different social media platforms.
* Most providers don't expose data.

#### [Redirect](https://www.drupal.org/project/redirect)
* Allows content editors to change content paths without breaking links.

#### [Token](https://www.drupal.org/project/token)
* Provides additional tokens not supported by core (most notably fields), as well as a UI for browsing tokens.
* Required by Entity Legal, Pathauto.

#### [Video Embed Field](https://www.drupal.org/project/video_embed_field)
* Creates a simple field type that allows you to embed videos from YouTube and Vimeo and show their thumbnail previews simply by entering the video's url.

### Themes

While Drutopia (as a platform) intends to leverage the Twig templating layer to enable site owners to have full control over their templates as well as CSS, the Drutopia distribution will include at least one theme and the Drutopia platform will invite further contributed themes that meet the following criteria:

* Responsive design
* Accessible contrasts for color blind and slightly visually impaired visitors