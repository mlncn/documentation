# Customizing & Styling Drutopia

**Sitebuilder and Themer Documentation**

## Introduction and clarification

Most day-to-day usage of Drutopia, including for [site managers](end-user-documentation.md#user-roles), is amply covered in [Drutopia's end-user documentation, "Using your Drutopia site").

This documentation is only for people who want to customize advanced configuration or create their own styles, or themes in Drupal parlance.
